export class SoilDescriptor {
  constructor() {
    this.greenColor = "green";
    this.redColor = "red";
    this.yellowColor = "yellow";

    this.values = [{
        "name": " Salinity, pH and texture",
        "members": [{
            "name": "Field Texture",
            "method": "Field ",
            "units": "na",
            "classes": []
          },
          {
            "name": "pH (water)",
            "method": "R&L 4A1",
            "units": "pH",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 5,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 5,
                "max": 6.5,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 6.5,
                "max": 7.5,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 7.5,
                "max": 8.6,
                "colour": "yellow"
              },
              {
                "name": "Very High",
                "min": 8.6,
                "max": null,
                "colour": "red"
              }
            ]
          },
          {
            "name": "pH (CaCl)",
            "method": "R&L 4B4",
            "units": "pH",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 5,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 5,
                "max": 6.5,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 6.5,
                "max": 7.5,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 7.5,
                "max": 8.6,
                "colour": "yellow"
              },
              {
                "name": "Very High",
                "min": 8.6,
                "max": null,
                "colour": "red"
              }
            ]
          },
          {
            "name": "Conductivity EC1:5",
            "method": "R&L 3A1",
            "units": "dS/m",
            "classes": [],
            "notes": "See ECe results below"
          },
          {
            "name": "Texture Factor",
            "method": "Calc",
            "units": "na",
            "classes": []
          },
          {
            "name": "Conductivity ECe",
            "method": "Calc",
            "units": "dS/m",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 2,
                "colour": "green"
              },
              {
                "name": "Low",
                "min": 2,
                "max": 4,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 4.01,
                "max": 8,
                "colour": "red"
              },
              {
                "name": "High",
                "min": 8,
                "max": 16,
                "colour": "red"
              },
              {
                "name": "Very High",
                "min": 16,
                "max": null,
                "colour": "red"
              }
            ]
          },
          {
            "name": "Clay Content",
            "method": "Calc",
            "units": "%",
            "classes": []
          },
          {
            "name": "Mineralogy",
            "method": "Calc",
            "units": "na",
            "classes": []
          },
        ]
      },
      {
        "name": "Exchangeable Cations",
        "members": [{
            "name": "Sodium (Na)",
            "method": "R&L 15E2",
            "units": "meq/100g",
            "classes": [{
                "name": "Very Low",
                "min": 0,
                "max": 0.1,
                "colour": "green"
              },
              {
                "name": "Low",
                "min": 0.1,
                "max": 0.3,
                "colour": "green"
              },
              {
                "name": "Moderate",
                "min": 0.3,
                "max": 0.7,
                "colour": "yellow"
              },
              {
                "name": "High",
                "min": 0.7,
                "max": 2,
                "colour": "red"
              },
              {
                "name": "Very High",
                "min": 2,
                "max": null,
                "colour": "red"
              }
            ]
          },
          {
            "name": "Potassium (K)",
            "method": "R&L 15E2",
            "units": "meq/100g",
            "classes": [{
                "name": "Very Low",
                "min": 0,
                "max": 0.2,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 0.2,
                "max": 0.3,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 0.3,
                "max": 0.7,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 0.7,
                "max": 2,
                "colour": "green"
              },
              {
                "name": "Very High",
                "min": 2,
                "max": null,
                "colour": "green"
              }
            ]
          },
          {
            "name": "Magnesium (Mg)",
            "method": "R&L 15E2",
            "units": "meq/100g",
            "classes": [{
                "name": "Very Low",
                "min": 0,
                "max": 0.3,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 0.3,
                "max": 1,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 1,
                "max": 3,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 3,
                "max": 8,
                "colour": "green"
              },
              {
                "name": "Very High",
                "min": 8,
                "max": null,
                "colour": "green"
              }
            ]
          },
          {
            "name": "Calcium (Ca)",
            "method": "R&L 15E2",
            "units": "meq/100g",
            "classes": [{
                "name": "Very Low",
                "min": 0,
                "max": 2,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 2,
                "max": 5,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 5,
                "max": 10,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 10,
                "max": 20,
                "colour": "green"
              },
              {
                "name": "Very High",
                "min": 20,
                "max": null,
                "colour": "green"
              }
            ]
          },
          {
            "name": "Aluminium (Al)",
            "method": "",
            "units": "meq/100g",
            "classes": [],
            "notes": "Dependent on pH and exchangeable Ca.Significant below pH 5.5"
          },
          {
            "name": "Cation Exchange Capacity ",
            "method": "R&L 15E2",
            "units": "meq/100g",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 6,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 6,
                "max": 12,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 12,
                "max": 25,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 25,
                "max": 40,
                "colour": "green"
              },
              {
                "name": "Very High",
                "min": 40,
                "max": null,
                "colour": "green"
              }
            ]
          },
          {
            "name": "ESP (calc)",
            "method": "Calc",
            "units": "%",
            "classes": [{
                "name": "Moderate",
                "min": null,
                "max": 6,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 6,
                "max": 14,
                "colour": "yellow"
              },
              {
                "name": "Very High",
                "min": 14,
                "max": null,
                "colour": "red"
              }
            ]
          },
          {
            "name": "Ca:Mg ratio",
            "method": "Calc",
            "units": "ratio",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 1,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 1,
                "max": 4,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 4,
                "max": 6,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 6,
                "max": 10,
                "colour": "yellow"
              },
              {
                "name": "Very High",
                "min": 10,
                "max": null,
                "colour": "red"
              }
            ]
          },
          {
            "name": "CEC to Clay Ratio (CCR)",
            "method": "Calc",
            "units": "ratio",
            "classes": []
          },
          {
            "name": "Gypsum requirement",
            "method": "Calc",
            "units": "t/ha",
            "classes": []
          }
        ]
      },
      {
        "name": "Organic Carbon",
        "members": [{
            "name": "Organic Carbon",
            "method": "R&L 6A1",
            "units": "%",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 0.6,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 0.6,
                "max": 1,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 1,
                "max": 1.74,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 1.74,
                "max": 3,
                "colour": "green"
              },
              {
                "name": "Very High",
                "min": 3,
                "max": null,
                "colour": "green"
              }
            ]
          },
          {
            "name": "Organic Matter",
            "method": "Calc",
            "units": "%",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 1,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 1,
                "max": 1.7,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 1.7,
                "max": 3,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 3,
                "max": 5.15,
                "colour": "green"
              },
              {
                "name": "Very High",
                "min": 5.15,
                "max": null,
                "colour": "green"
              }
            ]
          }
        ]
      },
      {
        "name": "Major Nutrients",
        "members": [{
            "name": "Ammonium",
            "method": "R&L 7C2b",
            "units": "mg/kg",
            "classes": []
          },
          {
            "name": "Nitrate Nitrogen (N)",
            "method": "R&L 7B1/7C2b",
            "units": "mg/kg",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 8,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 8,
                "max": 30,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 30,
                "max": null,
                "colour": "green"
              }
            ]
          },
          {
            "name": "Total Nitrogen",
            "method": "R&L 7A5(LECO)",
            "units": "mg/kg",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 500,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 500,
                "max": 1500,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 1500,
                "max": 2500,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 2500,
                "max": 5000,
                "colour": "green"
              },
              {
                "name": "Very High",
                "min": 5000,
                "max": null,
                "colour": "green"
              }
            ]
          },
          {
            "name": "Avaialble Phosphous (Colwell)",
            "method": "R&L 18A1",
            "units": "mg/kg",
            "classes": [],
            "classSet": [
              [{
                  "name": "Low",
                  "min": 0,
                  "max": 14,
                  "colour": "red"
                },
                {
                  "name": "Moderate",
                  "min": 14,
                  "max": 20,
                  "colour": "yellow"
                },
                {
                  "name": "High",
                  "min": 20,
                  "max": null,
                  "colour": "green"
                }
              ],
              [{
                  "name": "Low",
                  "min": 0,
                  "max": 16,
                  "colour": "red"
                },
                {
                  "name": "Moderate",
                  "min": 16,
                  "max": 30,
                  "colour": "yellow"
                },
                {
                  "name": "High",
                  "min": 30,
                  "max": null,
                  "colour": "green"
                }
              ],
              [{
                  "name": "Low",
                  "min": 0,
                  "max": 18,
                  "colour": "red"
                },
                {
                  "name": "Moderate",
                  "min": 18,
                  "max": 40,
                  "colour": "yellow"
                },
                {
                  "name": "High",
                  "min": 40,
                  "max": null,
                  "colour": "green"
                }
              ],
              [{
                  "name": "Low",
                  "min": 0,
                  "max": 30,
                  "colour": "red"
                },
                {
                  "name": "Moderate",
                  "min": 30,
                  "max": 80,
                  "colour": "yellow"
                },
                {
                  "name": "High",
                  "min": 80,
                  "max": null,
                  "colour": "green"
                }
              ]
            ]
          },
          {
            "name": "Avaialble Potassium (Colwell K)",
            "method": "R&L 18A1 ",
            "units": "mg/kg",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 78,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 78,
                "max": 117,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 117,
                "max": 273,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 273,
                "max": 780,
                "colour": "green"
              },
              {
                "name": "Very High",
                "min": 780,
                "max": null,
                "colour": "green"
              }
            ]
          },
          // {
          //   "name": "Total Sulphur",
          //   "method": "Acid Digest ICP-AES",
          //   "units": "mg/kg",
          //   "classes": [{
          //       "name": "Very Low",
          //       "min": null,
          //       "max": 50,
          //       "colour":"red"
          //     },
          //     {
          //       "name": "Low",
          //       "min": 50,
          //       "max": 200,
          //       "colour":"yellow"
          //     },
          //     {
          //       "name": "Moderate",
          //       "min": 200,
          //       "max": 500,
          //       "colour":"green"
          //     },
          //     {
          //       "name": "High",
          //       "min": 500,
          //       "max": 1000,
          //       "colour":"green"
          //     },
          //     {
          //       "name": "Very High",
          //       "min": 1000,
          //       "max": null,
          //       "colour":"green"
          //     }
          //   ]
          // },
          {
            "name": "Available Sulphur (KCL S)",
            "method": "R&L 10D1/10B3",
            "units": "mg/kg",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 4,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 4,
                "max": 8,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 8,
                "max": 12,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 12,
                "max": 20,
                "colour": "green"
              },
              {
                "name": "Very High",
                "min": 20,
                "max": null,
                "colour": "green"
              }
            ]
          },
          {
            "name": "C:N Ratio",
            "method": "Calc",
            "units": "ratio",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 25,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 15,
                "max": 25,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 0,
                "max": 15,
                "colour": "green"
              }
            ]
          }
        ]
      },
      {
        "name": "Trace Elements",
        "members": [{
            "name": "Boron (B)",
            "method": "R&L 12C1/C2",
            "units": "mg/kg",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 0.5,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 0.5,
                "max": 1,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 1,
                "max": 2,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 2,
                "max": 5,
                "colour": "green"
              },
              {
                "name": "Very High",
                "min": 5,
                "max": null,
                "colour": "green"
              }
            ]
          },
          {
            "name": "Available Zinc (Zn)",
            "method": "R&L 12A1",
            "units": "mg/kg",
            "classes": [],
            "classSet": [
              [{
                  "name": "Very Low",
                  "min": null,
                  "max": 0.3,
                  "colour": "red"
                },
                {
                  "name": "Low",
                  "min": 0.3,
                  "max": 0.8,
                  "colour": "yellow"
                },
                {
                  "name": "Moderate",
                  "min": 0.8,
                  "max": 5,
                  "colour": "green"
                },
                {
                  "name": "High",
                  "min": 5,
                  "max": 15,
                  "colour": "green"
                },
                {
                  "name": "Very High",
                  "min": 15,
                  "max": null,
                  "colour": "green"
                }
              ],
              [{
                  "name": "Very Low",
                  "min": null,
                  "max": 0.2,
                  "colour": "red"
                },
                {
                  "name": "Low",
                  "min": 0.2,
                  "max": 0.5,
                  "colour": "yellow"
                },
                {
                  "name": "Moderate",
                  "min": 0.5,
                  "max": 5,
                  "colour": "green"
                },
                {
                  "name": "High",
                  "min": 5,
                  "max": 15,
                  "colour": "green"
                },
                {
                  "name": "Very High",
                  "min": 15,
                  "max": null,
                  "colour": "green"
                }
              ]
            ]
          },
          {
            "name": "Iron (Fe)",
            "method": "R&L 12A1",
            "units": "mg/kg",
            "classes": []
          },
          {
            "name": "Copper (Cu)",
            "method": "R&L 12A1",
            "units": "mg/kg",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 0.1,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 0.1,
                "max": 0.3,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 0.3,
                "max": 5,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 5,
                "max": 15,
                "colour": "green"
              },
              {
                "name": "Very High",
                "min": 15,
                "max": null,
                "colour": "green"
              }
            ]
          },
          {
            "name": "Manganese (Mn)",
            "method": "R&L 12A1",
            "units": "mg/kg",
            "classes": [{
                "name": "Very Low",
                "min": null,
                "max": 1,
                "colour": "red"
              },
              {
                "name": "Low",
                "min": 1,
                "max": 2,
                "colour": "yellow"
              },
              {
                "name": "Moderate",
                "min": 2,
                "max": 50,
                "colour": "green"
              },
              {
                "name": "High",
                "min": 50,
                "max": 500,
                "colour": "green"
              },
              {
                "name": "Very High",
                "min": 500,
                "max": null,
                "colour": "green"
              }
            ]
          }
        ]
      }
    ]
  }

  setClass(soilDescriptor, sample, i, j) {

    var sampleMember = sample.values[i][j];
    var descriptor = soilDescriptor.values[i].members[j];
    var sampleTexture = sample.currentGrade.group.name;
    var samplePh = sample.pHWater.value;

    // Set the phosphorus classes by soil
    if (sampleTexture == "Sands" || sampleTexture == "Sandy Loam") {
      soilDescriptor.values[2].members[4].classes = soilDescriptor.values[2].members[4].classSet[0];
    } else if (sampleTexture == "Loams") {
      soilDescriptor.values[2].members[4].classes = soilDescriptor.values[2].members[4].classSet[1];
    } else if (sampleTexture == "Clay Loams") {
      soilDescriptor.values[2].members[4].classes = soilDescriptor.values[2].members[4].classSet[2];
    } else {
      soilDescriptor.values[2].members[4].classes = soilDescriptor.values[2].members[4].classSet[3];
    }

    // Set the zinc classes by ph
    if (samplePh >= 7) {
      soilDescriptor.values[3].members[1].classes = soilDescriptor.values[3].members[1].classSet[0];
    } else {
      thisoilDescriptors.values[3].members[1].classes = soilDescriptor.values[3].members[1].classSet[1];
    }

    // Do the testing
    if (descriptor.classes.length == 0) {
      sampleMember.colourClass = "";
      return;
    } else {
      for (var i = 0; i < descriptor.classes.length; i++) {
        if (descriptor.classes[i].min == null) {
          if (sampleMember.value < descriptor.classes[i].max) {
            sampleMember.colourClass = descriptor.classes[i].colour;
            return;
          }
        } else if (descriptor.classes[i].max == null) {
          if (sampleMember.value >= descriptor.classes[i].min) {
            sampleMember.colourClass = descriptor.classes[i].colour;
            return;
          }
        } else if (sampleMember.value > descriptor.classes[i].min && sampleMember.value <= descriptor.classes[i].max) {
          sampleMember.colourClass = descriptor.classes[i].colour;
          return;
        }
      }
    }
  }
}
