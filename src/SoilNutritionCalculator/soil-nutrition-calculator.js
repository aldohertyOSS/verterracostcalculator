import {
  inject
} from 'aurelia-framework';
import {
  bindable
} from 'aurelia-framework';
import {
  NutrientRequirements
} from './nutrient-requirements';
import {
  Assumptions
} from './assumptions';
import {
  SoilTextures
} from './soil-textures';
import {
  SoilDescriptor
} from './soil-descriptor';
import {
  SoilSample
} from './soil-sample';
import {
  ObserverLocator
} from 'aurelia-framework';

@inject(ObserverLocator)

export class SoilNutritionCalculator {
  // @bindable currentTexture;
  // @bindable currentGrade;

  constructor(observerLocator) {

    this.observerLocator = observerLocator;
    this.assumptions = [];
    this.assumptions = new Assumptions();
    this.soilTextures = new SoilTextures();

    this.soilSamples = [];
    for (var i = 0; i < 3; i++) {
      this.soilSamples.push(new SoilSample());
      // this.assumptions.push(new Assumptions());
    }


    //this.soilDescriptor = new SoilDescriptor();
    // this.currentGrade = this.soilTextures.values[0];
  }

  // currentGradeChanged() {
  //     this.calcSoilSample()
  // }

  addToAssumptionsWatch(element) {
    var subscription = this.observerLocator
      .getObserver(element, 'value')
      .subscribe((newValue, oldValue) => this.onAssumptionsChange(newValue, oldValue));
  }

  addToSoilsWatch(element) {
    var subscription = this.observerLocator
      .getObserver(element, 'value')
      .subscribe((newValue, oldValue) => this.onSampleChange(newValue, oldValue));
  }

  formatValue(value, sigFigs) {
    var val = value.toFixed(sigFigs);

    return val.toLocaleString();

  }

  calcSoilSamples() {
    //this.updateClasses();
    this.updating = true;
    this.soilSamples.forEach(function (element, i) {
      element.values.forEach(function (section) {
        section.members.forEach(function (measure) {
          if (measure.nonNumeric === undefined) {
            measure.value = Number(measure.value);
          }
        })
      })
    })

   

    this.soilSamples.forEach(function (element, i) {
      element.calcNutrients(this.assumptions, i);
      element.calcGypsReq(this.assumptions, i);
      element.setClasses();
    }, this);
    // this.soilSample.calcNutrients(this.assumptions);
    // this.soilSample.calcGypsReq(this.assumptions);
    this.updating = false;
  }

  onSampleChange(newValue, oldValue) {
    if (this.updating != true) {
      this.calcSoilSamples();
    }
  }

  onAssumptionsChange(newValue, oldValue) {
    this.assumptions.calcVolumes();
    this.calcSoilSamples();
  }

  updateClasses() {
    this.soilSamples.forEach(function (sample) {
      for (var i = 0; i < this.soilDescriptor.values.length; i++) {
        for (var j = 0; j < this.soilDescriptor.values[i].members.length; j++) {
          this.soilDescriptor.setClass(this.soilDescriptor, sample, i, j);
        }
      }
    }, this);
  }

  attached() {
    this.assumptions.values.forEach(function (group) {
      group.vars.forEach(function (element) {
        if (element.input == true)
          this.addToAssumptionsWatch(element);
      }, this);
    }, this);

    // this.soilSample.values.forEach(function (group) {
    //     group.forEach(function (element) {
    //         if (element.input == true)
    //             this.addToSoilsWatch(element);
    //     }, this);
    // }, this);

    this.soilSamples.forEach(function (sample) {
      sample.values.forEach(function (group) {
        group.members.forEach(function (element) {
          if (element.input == true)
            this.addToSoilsWatch(element);
        }, this);
      }, this);
    }, this);

    // Watch the soils changed
    this.soilSamples.forEach(function (element) {
      this.observerLocator
        .getObserver(element, 'currentGrade')
        .subscribe((newValue, oldValue) => this.onSampleChange(newValue, oldValue));
    }, this);
  }
}
