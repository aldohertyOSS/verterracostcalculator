
export class NutrientRequirements {
  constructor(ph, soilTexture) {
    this.sodium = {
      "threshold": null,
      "mwc": null
    };

    this.potassium = {
      "threshold": 0.3,
      "mwc": 39
    };

    this.magnesium = {
      "threshold": 1,
      "mwc": 12
    };

    this.calcium = {
      "threshold": 5.0,
      "mwc": 20
    };

    this.organicCarbon = {
      "threshold": 0.98837,
      "mwc": null
    };

    this.organicMatter = {
      "threshold": 1.7,
      "mwc": null
    };

    this.ammonium = {
      "threshold": 30,
      "mwc": null
    };

    this.nitrateN = {
      "threshold": 30,
      "mwc": null
    };

    this.totalN = {
      "threshold": 1500,
      "mwc": null
    };

    this.phosphorusAvail = {
      "threshold": 70,
      "mwc": null
    };

    if (soilTexture = "Sandy Loams") {
      this.phosphorusAvail.threshold = 20;
    }
    if (soilTexture = "Loams") {
      this.phosphorusAvail.threshold = 30;
    }
    if (soilTexture = "Clay Loams") {
      this.phosphorusAvail.threshold = 40;
    }
    if (soilTexture = "Heavy Clays") {
      this.phosphorusAvail.threshold = 80;
    }

    this.potassiumAvail = {
      "threshold": 117,
      "mwc": null
    };

    this.sulphurAvail = {
      "threshold": 8,
      "mwc": null
    };

    this.boron = {
      "threshold": 1,
      "mwc": null
    };
    this.zinc = {
      "threshold": 0.8,
      "mwc": null
    };

    if (ph < 7) {
      this.zinc.threshold = 0.5;
    }

    this.copper = {
      "threshold": 0.3,
      "mwc": null
    };
    this.manganese = {
      "threshold": 2,
      "mwc": null
    };

    // this.values = [{
    //     "group": "Exchangeable Cations", "vars": [
    //         { "name": "Sodium (Na)", "threshold": null, "units": "meq/100g", "mwc": null },
    //         { "name": "Potassium (K)", "threshold": 0.8, "units": "meq/100g", "mwc": 39 },
    //         { "name": "Magnesium (Mg)", "threshold": 2.5, "units": "meq/100g", "mwc": 12 },
    //         { "name": "Calcium (Ca)", "threshold": 15.0, "units": "meq/100g", "mwc": 20 }]
    // },
    // {
    //     "group": "Major Nutrients and Organic Carbon", "vars": [
    //         { "name": "Nitrate Nitrogen", "threshold": 30, "units": "mg/kg", "mwc": null },
    //         { "name": "Total Nitrogen (N)", "threshold": 1500, "units": "mg/kg", "mwc": null },
    //         { "name": "Available Phosphorus (P)", "threshold": 20, "units": "mg/kg", "mwc": null },
    //         { "name": "Organic Carbon", "threshold": 2, "units": "%", "mwc": null },
    //         { "name": "Sulphate", "threshold": 8, "units": "mg/kg", "mwc": null }]
    // },
    // {
    //     "group": "Trace Elements", "vars": [
    //         { "name": "Boron (B)", "threshold": 1, "units": "mg/kg", "mwc": null },
    //         { "name": "Available Zinc (Zn) pH > 7", "threshold": 0.8, "units": "mg/kg", "mwc": null },
    //         { "name": "Available Zinc (Zn) pH < 7", "threshold": 0.5, "units": "mg/kg", "mwc": null },
    //         { "name": "Copper (Cu)", "threshold": 0.3, "units": "mg/kg", "mwc": null },
    //         { "name": "Manganese (Mn)", "threshold": 2, "units": "mg/kg", "mwc": null }]
    // }];
  }
}
