export class Assumptions {
  constructor() {

    //General
    this.bulkDensity = {
      "description": "Soil bulk density",
      "units": "g/cm3",
      "value": 1.3,
      "input": true
    };

    //Nutrients and Organic matter
    this.soilDepthNutrients = {
      "description": "Soil depth to be treated for nutrients",
      "units": "cm",
      "value": 15,
      "input": true
    };
    this.targetOrganicMatterThreshold = {
      "description": "Target Organic Matter Threshold",
      "units": "%",
      "value": 1.7,
      "input": true
    };
    this.soilMassNutrients = {
      "description": "Soil mass to be treated for nutrients",
      "units": "t/ha",
      "value": 1950,
      "input": false
    };

    //Sodicity
    this.soilDepthSodicity = {
      "description": "Soil depth to be treated for Sodicity Layer ",
      "units": "cm",
      //"value": 15,
      "value": [15,20,0],
      "input": true
    };
    // this.soilDepthSodicity2 = {
    //   "description": "Soil depth to be treated for Sodicity Layer 2",
    //   "units": "cm",
    //   "value": 20,
    //   "input": true
    // };
    // this.soilDepthSodicity3 = {
    //   "description": "Soil depth to be treated for Sodicity  Layer 3",
    //   "units": "cm",
    //   "value": 0,
    //   "input": true
    // };
    this.gypsumPurity = {
      "description": "Gypsum Purity",
      "units": "%",
      "value": 90,
      "input": true
    };
    this.gypsumEfficiencyFactor = {
      "description": "Gypsum Efficiency factor",
      "units": "factor",
      "value": 1,
      "input": true
    };
    this.targetESP = {
      "description": "Target ESP",
      "units": "%",
      "value": 5,
      "input": true
    };
    this.soilMassSodicity = {
      "description": "Soil mass to be treated for sodicity Layer ",
      "units": "t/ha",
      "value": [1950, 2600, 0],
      "input": false
    };
    // this.soilMassSodicity2 = {
    //   "description": "Soil mass to be treated for sodicity Layer 2",
    //   "units": "t/ha",
    //   "value": 2600,
    //   "input": false
    // };
    // this.soilMassSodicity3 = {
    //   "description": "Soil mass to be treated for sodicity Layer 3",
    //   "units": "t/ha",
    //   "value": 0,
    //   "input": false
    // };

    //Add to values for repeaters
    this.values = [{
        "category": "Assumptions - General ",
        "vars": [
          this.bulkDensity
        ]
      }, {
        "category": "Assumptions - Nutrients and Organic Matter",
        "vars": [
          this.soilDepthNutrients,
          this.targetOrganicMatterThreshold,
          this.soilMassNutrients
        ]
      },
      {
        "category": "Assumptions - Sodicity",
        "vars": [
          this.soilDepthSodicity,
          // this.soilDepthSodicity1,
          // this.soilDepthSodicity2,
          // this.soilDepthSodicity3,
          this.gypsumPurity,
          this.gypsumEfficiencyFactor,
          this.targetESP,
          this.soilMassSodicity,
          // this.soilMassSodicity1,
          // this.soilMassSodicity2,
          // this.soilMassSodicity3
        ]
      }
    ];

    this.calcVolumes();
  }

  calcVolumes() {
    this.soilMassNutrients.value = 10000 * this.soilDepthNutrients.value / 100 * this.bulkDensity.value;
    
    for(var i = 0; i < this.soilMassSodicity.value.length; i++)
    {
      this.soilMassSodicity.value[i] = 10000 * this.soilDepthSodicity.value[i] / 100 * this.bulkDensity.value;
    }
    // this.soilMassSodicity1.value = 10000 * this.soilDepthSodicity1.value / 100 * this.bulkDensity.value;
    // this.soilMassSodicity2.value = 10000 * this.soilDepthSodicity2.value / 100 * this.bulkDensity.value;
    // this.soilMassSodicity3.value = 10000 * this.soilDepthSodicity3.value / 100 * this.bulkDensity.value;
  }

}
