export class Mineralogy {
    constructor() {
        this.values = [
            { "mineralogy": "Kaolinite", "code": "K", "CCRLow": 0, "CCRHigh": 0.2 },
            { "mineralogy": "Kaolinite and Illite", "code": "K+I", "CCRLow": 0.2, "CCRHigh": 0.35 },
            { "mineralogy": "Mixed Mineralogy", "code": "MM", "CCRLow": 0.35, "CCRHigh": 0.55 },
            { "mineralogy": "Mixed Mineralogy with a high propotin of Montmorillonite", "code": "MM+M", "CCRLow": 0.55, "CCRHigh": 0.95 },
            { "mineralogy": "Montmorillonite plus feldspars and/or CEC other than clay fraction", "code": "M+F", "CCRLow": 0.95, "CCRHigh": 2 }
        ]
    }
}