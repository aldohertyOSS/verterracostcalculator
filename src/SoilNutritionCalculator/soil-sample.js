import {
  SoilTextures
} from "./soil-textures";
import {
  Mineralogy
} from "./mineralogy";
import {
  NutrientRequirements
} from "./nutrient-requirements";

export class SoilSample {
  constructor() {

    // this.greenColor = "green";
    // this.redColor = "red";
    // this.yellowColor = "yellow";

    this.currentGrade = null;

    this.NAValue = "na";

    this.soilTextures = new SoilTextures();
    this.mineralogy = new Mineralogy();

    this.currentGrade = this.soilTextures.values[0];
    this.currentMineralogy = this.mineralogy.values[0];

    this.fieldTexture = {
      "name": "Field Texture",
      "method": "Field ",
      "nonNumeric": true,
      "units": "na",
      "classes": []
    };

    this.pHWater = {
      "value": 0,
      "input": true,
      "name": "pH (water)",
      "method": "R&L 4A1",
      "units": "pH",
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 5,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 5,
          "max": 6.5,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 6.5,
          "max": 7.5,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 7.5,
          "max": 8.6,
          "colour": "yellow"
        },
        {
          "name": "Very High",
          "min": 8.6,
          "max": null,
          "colour": "red"
        }
      ]
    };

    this.pHCaCl = {
      "value": 0,
      "input": true,
      "name": "pH (CaCl)",
      "method": "R&L 4B4",
      "units": "pH",
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 5,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 5,
          "max": 6.5,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 6.5,
          "max": 7.5,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 7.5,
          "max": 8.6,
          "colour": "yellow"
        },
        {
          "name": "Very High",
          "min": 8.6,
          "max": null,
          "colour": "red"
        }
      ]
    };

    this.conductivity = {
      "value": 0,
      "input": true,
      "name": "Conductivity EC1:5",
      "method": "R&L 3A1",
      "units": "dS/m",
      "classes": [],
      "notes": "See ECe results below"
    };

    this.textureFactor = {
      "value": 0,
      "input": false,
      "sigFigs": 1,
      "nonNumeric": true,
      "name": "Texture Factor",
      "method": "Calc",
      "units": "na",
      "classes": []
    };

    this.conductivityECe = {
      "value": 0,
      "input": false,
      "sigFigs": 1,
      "name": "Conductivity ECe",
      "method": "Calc",
      "units": "dS/m",
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 2,
          "colour": "green"
        },
        {
          "name": "Low",
          "min": 2,
          "max": 4,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 4.01,
          "max": 8,
          "colour": "red"
        },
        {
          "name": "High",
          "min": 8,
          "max": 16,
          "colour": "red"
        },
        {
          "name": "Very High",
          "min": 16,
          "max": null,
          "colour": "red"
        }
      ]
    };

    this.clayContent = {
      "value": 0,
      "input": false,
      "sigFigs": 2,
      "name": "Clay Content",
      "method": "Calc",
      "units": "%",
      "classes": []
    };

    this.mineralogyName = {
      "value": "",
      "input": false,
      "name": "Mineralogy",
      "method": "Calc",
      "units": "na",
      "classes": []
    };

    this.sodium = {
      "units": "meq/100g",
      "value": 0,
      "description": "Exch. Sodium",
      "input": true,
      "name": "Sodium (Na)",
      "method": "R&L 15E2",
      "units": "meq/100g",
      "classes": [{
          "name": "Very Low",
          "min": 0,
          "max": 0.1,
          "colour": "green"
        },
        {
          "name": "Low",
          "min": 0.1,
          "max": 0.3,
          "colour": "green"
        },
        {
          "name": "Moderate",
          "min": 0.3,
          "max": 0.7,
          "colour": "yellow"
        },
        {
          "name": "High",
          "min": 0.7,
          "max": 2,
          "colour": "red"
        },
        {
          "name": "Very High",
          "min": 2,
          "max": null,
          "colour": "red"
        }
      ]
    };

    this.potassium = {
      "units": "meq/100g",
      "value": 0,
      "description": ". Potassium",
      "input": true,
      "showRequirements": 1,
      "name": "Potassium (K)",
      "method": "R&L 15E2",
      "units": "meq/100g",
      "classes": [{
          "name": "Very Low",
          "min": 0,
          "max": 0.2,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 0.2,
          "max": 0.3,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 0.3,
          "max": 0.7,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 0.7,
          "max": 2,
          "colour": "green"
        },
        {
          "name": "Very High",
          "min": 2,
          "max": null,
          "colour": "green"
        }
      ]
    };

    this.magnesium = {
      "units": "meq/100g",
      "value": 0,
      "description": ". Magnesium",
      "input": true,
      "showRequirements": 1,
      "name": "Magnesium (Mg)",
      "method": "R&L 15E2",
      "units": "meq/100g",
      "classes": [{
          "name": "Very Low",
          "min": 0,
          "max": 0.3,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 0.3,
          "max": 1,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 1,
          "max": 3,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 3,
          "max": 8,
          "colour": "green"
        },
        {
          "name": "Very High",
          "min": 8,
          "max": null,
          "colour": "green"
        }
      ]
    };

    this.calcium = {
      "units": "meq/100g",
      "value": 0,
      "description": ". Calcium",
      "input": true,
      "name": "Calcium (Ca)",
      "showRequirements": 2,
      "method": "R&L 15E2",
      "units": "meq/100g",
      "classes": [{
          "name": "Very Low",
          "min": 0,
          "max": 2,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 2,
          "max": 5,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 5,
          "max": 10,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 10,
          "max": 20,
          "colour": "green"
        },
        {
          "name": "Very High",
          "min": 20,
          "max": null,
          "colour": "green"
        }
      ]
    };

    this.aluminium = {
      "units": "meq/100g",
      "value": 0,
      "description": ". Aluminium",
      "input": true,
      "name": "Aluminium (Al)",
      "method": "",
      "units": "meq/100g",
      "classes": [],
      "notes": "Dependent on pH and exchangeable Ca.Significant below pH 5.5"
    };

    this.CEC = {
      "units": "meq/100g",
      "value": 0,
      "description": "CEC",
      "input": false,
      "sigFigs": 2,
      "name": "Cation Exchange Capacity ",
      "method": "R&L 15E2",
      "units": "meq/100g",
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 6,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 6,
          "max": 12,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 12,
          "max": 25,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 25,
          "max": 40,
          "colour": "green"
        },
        {
          "name": "Very High",
          "min": 40,
          "max": null,
          "colour": "green"
        }
      ]
    };

    this.ESP = {
      "units": "%",
      "value": 0,
      "description": "ESP",
      "input": false,
      "sigFigs": 2,
      "name": "ESP (calc)",
      "method": "Calc",
      "units": "%",
      "classes": [{
          "name": "Moderate",
          "min": null,
          "max": 6,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 6,
          "max": 14,
          "colour": "yellow"
        },
        {
          "name": "Very High",
          "min": 14,
          "max": null,
          "colour": "red"
        }
      ]
    };

    this.CaMgRatio = {
      "value": 0,
      "input": false,
      "sigFigs": 1,
      "name": "Ca:Mg ratio",
      "method": "Calc",
      "units": "ratio",
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 1,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 1,
          "max": 4,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 4,
          "max": 6,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 6,
          "max": 10,
          "colour": "yellow"
        },
        {
          "name": "Very High",
          "min": 10,
          "max": null,
          "colour": "red"
        }
      ]
    };

    this.CCR = {
      "value": 0,
      "input": false,
      "sigFigs": 2,
      "name": "CEC to Clay Ratio (CCR)",
      "method": "Calc",
      "units": "ratio",
      "classes": []
    };

    this.massCommericalGypsum = {
      "units": "t/ha",
      "value": 0,
      "description": "Mass of  commerical gypsum product",
      "input": false,
      "sigFigs": 2,
      "name": "Gypsum requirement",
      "method": "Calc",
      "units": "t/ha",
      "classes": []
    };

    this.organicCarbon = {
      "value": 0,
      "input": true,
      "name": "Organic Carbon",
      "showRequirements": 1,
      "method": "R&L 6A1",
      "units": "%",
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 0.6,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 0.6,
          "max": 1,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 1,
          "max": 1.74,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 1.74,
          "max": 3,
          "colour": "green"
        },
        {
          "name": "Very High",
          "min": 3,
          "max": null,
          "colour": "green"
        }
      ]
    };

    this.organicMatter = {
      "value": 0,
      "input": true,
      "name": "Organic Matter",
      "showRequirements": 1,
      "method": "Calc",
      "units": "%",
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 1,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 1,
          "max": 1.7,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 1.7,
          "max": 3,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 3,
          "max": 5.15,
          "colour": "green"
        },
        {
          "name": "Very High",
          "min": 5.15,
          "max": null,
          "colour": "green"
        }
      ]
    };

    this.ammonium = {
      "value": 0,
      "input": true,
      "name": "Ammonium",
      "method": "R&L 7C2b",
      "units": "mg/kg",
      "classes": []
    };

    this.nitrateN = {
      "value": 0,
      "input": true,
      "name": "Nitrate Nitrogen (N)",
      "method": "R&L 7B1/7C2b",
      "showRequirements": 1,
      "units": "mg/kg",
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 8,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 8,
          "max": 30,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 30,
          "max": null,
          "colour": "green"
        }
      ]
    };

    this.totalN = {
      "value": 0,
      "input": true,
      "name": "Total Nitrogen",
      "method": "R&L 7A5(LECO)",
      "showRequirements": 1,
      "units": "mg/kg",
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 500,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 500,
          "max": 1500,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 1500,
          "max": 2500,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 2500,
          "max": 5000,
          "colour": "green"
        },
        {
          "name": "Very High",
          "min": 5000,
          "max": null,
          "colour": "green"
        }
      ]
    };

    this.phosphorusAvail = {
      "value": 0,
      "input": true,
      "name": "Avaialble Phosphous (Colwell)",
      "method": "R&L 18A1",
      "showRequirements": 1,
      "units": "mg/kg",
      "classes": [],
      "classSet": [
        [{
            "name": "Low",
            "min": 0,
            "max": 14,
            "colour": "red"
          },
          {
            "name": "Moderate",
            "min": 14,
            "max": 20,
            "colour": "yellow"
          },
          {
            "name": "High",
            "min": 20,
            "max": null,
            "colour": "green"
          }
        ],
        [{
            "name": "Low",
            "min": 0,
            "max": 16,
            "colour": "red"
          },
          {
            "name": "Moderate",
            "min": 16,
            "max": 30,
            "colour": "yellow"
          },
          {
            "name": "High",
            "min": 30,
            "max": null,
            "colour": "green"
          }
        ],
        [{
            "name": "Low",
            "min": 0,
            "max": 18,
            "colour": "red"
          },
          {
            "name": "Moderate",
            "min": 18,
            "max": 40,
            "colour": "yellow"
          },
          {
            "name": "High",
            "min": 40,
            "max": null,
            "colour": "green"
          }
        ],
        [{
            "name": "Low",
            "min": 0,
            "max": 30,
            "colour": "red"
          },
          {
            "name": "Moderate",
            "min": 30,
            "max": 80,
            "colour": "yellow"
          },
          {
            "name": "High",
            "min": 80,
            "max": null,
            "colour": "green"
          }
        ]
      ]
    };

    this.potassiumAvail = {
      "value": 0,
      "input": true,
      "name": "Avaialble Potassium (Colwell K)",
      "method": "R&L 18A1 ",
      "showRequirements": 2,
      "units": "mg/kg",
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 78,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 78,
          "max": 117,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 117,
          "max": 273,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 273,
          "max": 780,
          "colour": "green"
        },
        {
          "name": "Very High",
          "min": 780,
          "max": null,
          "colour": "green"
        }
      ]
    };

    this.potassiumAvail.input = true;

    // {
    //   "name": "Total Sulphur",
    //   "method": "Acid Digest ICP-AES",
    //   "units": "mg/kg",
    //   "classes": [{
    //       "name": "Very Low",
    //       "min": null,
    //       "max": 50,
    //       "colour":"red"
    //     },
    //     {
    //       "name": "Low",
    //       "min": 50,
    //       "max": 200,
    //       "colour":"yellow"
    //     },
    //     {
    //       "name": "Moderate",
    //       "min": 200,
    //       "max": 500,
    //       "colour":"green"
    //     },
    //     {
    //       "name": "High",
    //       "min": 500,
    //       "max": 1000,
    //       "colour":"green"
    //     },
    //     {
    //       "name": "Very High",
    //       "min": 1000,
    //       "max": null,
    //       "colour":"green"
    //     }
    //   ]
    // }


    this.sulphurAvail = {
      "value": 0,
      "input": true,
      "name": "Available Sulphur (KCL S)",
      "method": "R&L 10D1/10B3",
      "units": "mg/kg",
      "showRequirements": 2,
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 4,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 4,
          "max": 8,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 8,
          "max": 12,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 12,
          "max": 20,
          "colour": "green"
        },
        {
          "name": "Very High",
          "min": 20,
          "max": null,
          "colour": "green"
        }
      ]
    };

    this.CNRatio = {
      "value": 0,
      "input": false,
      "sigFigs": 1,
      "name": "C:N Ratio",
      "method": "Calc",
      "units": "ratio",
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 25,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 15,
          "max": 25,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 0,
          "max": 15,
          "colour": "green"
        }
      ]
    };

    this.boron = {
      "value": 0,
      "input": true,
      "name": "Boron (B)",
      "method": "R&L 12C1/C2",
      "units": "mg/kg",
      "showRequirements": 2,
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 0.5,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 0.5,
          "max": 1,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 1,
          "max": 2,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 2,
          "max": 5,
          "colour": "green"
        },
        {
          "name": "Very High",
          "min": 5,
          "max": null,
          "colour": "green"
        }
      ]
    };

    this.zinc = {
      "value": 0,
      "input": true,
      "name": "Available Zinc (Zn)",
      "method": "R&L 12A1",
      "units": "mg/kg",
      "showRequirements": 2,
      "classes": [],
      "classSet": [
        [{
            "name": "Very Low",
            "min": null,
            "max": 0.3,
            "colour": "red"
          },
          {
            "name": "Low",
            "min": 0.3,
            "max": 0.8,
            "colour": "yellow"
          },
          {
            "name": "Moderate",
            "min": 0.8,
            "max": 5,
            "colour": "green"
          },
          {
            "name": "High",
            "min": 5,
            "max": 15,
            "colour": "green"
          },
          {
            "name": "Very High",
            "min": 15,
            "max": null,
            "colour": "green"
          }
        ],
        [{
            "name": "Very Low",
            "min": null,
            "max": 0.2,
            "colour": "red"
          },
          {
            "name": "Low",
            "min": 0.2,
            "max": 0.5,
            "colour": "yellow"
          },
          {
            "name": "Moderate",
            "min": 0.5,
            "max": 5,
            "colour": "green"
          },
          {
            "name": "High",
            "min": 5,
            "max": 15,
            "colour": "green"
          },
          {
            "name": "Very High",
            "min": 15,
            "max": null,
            "colour": "green"
          }
        ]
      ]
    };

    this.iron = {
      "units": "meq/100g",
      "value": 0,
      "description": ". Iron",
      "input": true,
      "showRequirements": 2,
      "name": "Iron (Fe)",
      "method": "R&L 12A1",
      "units": "mg/kg",
      "classes": []
    };

    this.copper = {
      "value": 0,
      "input": true,
      "name": "Copper (Cu)",
      "method": "R&L 12A1",
      "units": "mg/kg",
      "showRequirements": 2,
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 0.1,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 0.1,
          "max": 0.3,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 0.3,
          "max": 5,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 5,
          "max": 15,
          "colour": "green"
        },
        {
          "name": "Very High",
          "min": 15,
          "max": null,
          "colour": "green"
        }
      ]
    };

    this.manganese = {
      "value": 0,
      "input": true,
      "name": "Manganese (Mn)",
      "method": "R&L 12A1",
      "units": "mg/kg",
      "showRequirements": 2,
      "classes": [{
          "name": "Very Low",
          "min": null,
          "max": 1,
          "colour": "red"
        },
        {
          "name": "Low",
          "min": 1,
          "max": 2,
          "colour": "yellow"
        },
        {
          "name": "Moderate",
          "min": 2,
          "max": 50,
          "colour": "green"
        },
        {
          "name": "High",
          "min": 50,
          "max": 500,
          "colour": "green"
        },
        {
          "name": "Very High",
          "min": 500,
          "max": null,
          "colour": "green"
        }
      ]
    };


    this.EKP = {
      "units": "%",
      "value": 0,
      "description": "EKP",
      "input": false,
      "sigFigs": 2
    };

    this.ECaP = {
      "units": "%",
      "value": 0,
      "description": "ECaP",
      "input": false,
      "sigFigs": 2
    };

    this.EmgP = {
      "units": "%",
      "value": 0,
      "description": "EmgP",
      "input": false,
      "sigFigs": 2
    };

    this.EAlP = {
      "units": "%",
      "value": 0,
      "description": "EAlP",
      "input": false,
      "sigFigs": 2
    };

    this.EFeP = {
      "units": "%",
      "value": 0,
      "description": "EFeP",
      "input": false,
      "sigFigs": 2
    };

    this.NaExchTarget = {
      "units": " meq/100g",
      "value": 0,
      "description": "Target Exch Na (meq/100g)",
      "input": false,
      "sigFigs": 2
    };

    this.NaReduction = {
      "units": " meq/100g",
      "value": 0,
      "description": "Na reduction",
      "input": false,
      "sigFigs": 2
    };

    this.meqNaTreated = {
      "units": "meq/ha",
      "value": 0,
      "description": "meq Na to be treated",
      "input": false,
      "sigFigs": 2
    };

    this.molesNaTreated = {
      "units": "moles/ha",
      "value": 0,
      "description": "Moles Na to be treated",
      "input": false,
      "sigFigs": 2
    };

    this.molesCaNeeded = {
      "units": "moles/ha",
      "value": 0,
      "description": "Moles Ca to be added",
      "input": false,
      "sigFigs": 2
    };

    this.molecularWeightGypsum = {
      "units": "MW",
      "value": 0,
      "description": "Molecular Weight gypsum (CaSO4.2H20)",
      "input": false,
      "sigFigs": 2
    };

    this.weightGypsum = {
      "units": "g/ha",
      "value": 0,
      "description": "Weight gypsum",
      "input": false,
      "sigFigs": 2
    };

    this.massPureGypsum = {
      "units": "t/ha",
      "value": 0,
      "description": "Mass of pure gypsum, 100% efficiecny",
      "input": false,
      "sigFigs": 2
    };

    this.sulphate = {
      "value": 0,
      "input": true
    };


    // this.sodiumReq = {
    //   "value": 0,
    //   "input": false,
    //   "sigFigs": 0
    // };

    // this.magnesiumReq = {
    //   "value": 0,
    //   "input": false,
    //   "sigFigs": 0
    // };
    // this.calciumReq = {
    //   "value": 0,
    //   "input": false,
    //   "sigFigs": 0
    // };
    // this.nitrateNReq = {
    //   "value": 0,
    //   "input": false,
    //   "sigFigs": 0
    // };
    // this.totalNReq = {
    //   "value": 0,
    //   "input": false,
    //   "sigFigs": 0
    // };
    // this.phosphorusReq = {
    //   "value": 0,
    //   "input": false,
    //   "sigFigs": 1
    // };
    // this.organicCarbonReq = {
    //   "value": 0,
    //   "input": false,
    //   "sigFigs": 0
    // };
    // this.sulphateReq = {
    //   "value": 0,
    //   "input": false,
    //   "sigFigs": 1
    // };
    // this.boronReq = {
    //   "value": 0,
    //   "input": false,
    //   "sigFigs": 1
    // };
    // this.zincReq = {
    //   "value": 0,
    //   "input": false,
    //   "sigFigs": 1
    // };
    // this.copperReq = {
    //   "value": 0,
    //   "input": false,
    //   "sigFigs": 1
    // };
    // this.manganeseReq = {
    //   "value": 0,
    //   "input": false,
    //   "sigFigs": 1,
    // };


    this.values = [{
        "name": "Salinity, pH and texture",
        "members": [
          this.fieldTexture,
          this.pHWater,
          this.pHCaCl,
          this.conductivity,
          this.textureFactor,
          this.conductivityECe,
          this.clayContent,
          this.mineralogyName,
        ]
      },
      {
        "name": "Exchangeable Cations",
        "members": [

          this.sodium,
          this.potassium,
          this.magnesium,
          this.calcium,
          this.aluminium,
          this.CEC,
          this.ESP,
          this.CaMgRatio,
          this.CCR,
          this.massCommericalGypsum,
        ]
      },
      {
        "name": "Organic Carbon",
        "members": [
          this.organicCarbon,
          this.organicMatter
        ]
      },
      {
        "name": "Major Nutrients",
        "members": [
          this.ammonium,
          this.nitrateN,
          this.totalN,
          this.phosphorusAvail,
          this.potassiumAvail,
          this.sulphurAvail,
          this.CNRatio,
        ]
      },
      {
        "name": "Trace Elements",
        "members": [
          this.boron,
          this.zinc,
          // this.iron,
          this.copper,
          this.manganese
        ]
      }
    ];


    this.values.forEach(function (group) {
      group.members.forEach(function (element) {
        element.colourClass = "";
        element.required = 0;
      }, this);
    }, this);


    // Test Values   
    //Field texture - Heavy clay

    this.pHWater.value = 4.9; //4.9	6	7  
    this.pHCaCl.value = 4.9
    this.conductivity.value = 1.0; //1.0	0.5	0.3    


    //this.textureFactor.value = 5.8; //5.8	5.8	5.8
    this.sodium.value = 0.1; //0.1	0.31	0.71
    this.potassium.value = 0.1; //0.1	0.25	0.4
    this.magnesium.value = 0.2; //0.2	0.31	1.1   
    this.calcium.value = 5; //5	2.1	5.1 
    this.aluminium.value = 0.0;
    this.CEC.value = 5.8; //5.40	2.97	7.31
    //ESP   - 1.9%	10.4%	14.1%
    //CA:Mg ratio - 25.0	6.8	4.6
    //Gypsum req method 2 0.0	0.4	0.0

    this.organicCarbon.value = 0.57;
    this.organicMatter.value = 0.98;
    this.ammonium.value = 0.7;
    this.nitrateN.value = 7;
    this.totalN.value = 600;

    this.phosphorusAvail.value = 14;
    this.potassiumAvail.value = 78;

    this.sulphurAvail.value = 3.9;
    this.CNRatio.value = 9.5;

    this.boron.value = 4;
    this.zinc.value = 0.19;
    this.iron.value = 0;
    this.copper.value = 0.09;
    this.manganese.value = 0.9;

  }

  calcGypsReq(assumptions, layer) {
    this.CEC.value = this.sodium.value + this.potassium.value + this.magnesium.value + this.calcium.value;
    this.ESP.value = this.CEC.value != 0 ? this.sodium.value / this.CEC.value * 100 : 0;
    this.EKP.value = this.CEC.value != 0 ? this.potassium.value / this.CEC.value : 0;
    this.ECaP.value = this.CEC.value != 0 ? this.calcium.value / this.CEC.value : 0;
    this.EmgP.value = this.CEC.value != 0 ? this.magnesium.value / this.CEC.value : 0;
    this.EAlP.value = this.CEC.value != 0 ? this.aluminium.value / this.CEC.value : 0;
    this.EFeP.value = this.CEC.value != 0 ? this.iron.value / this.CEC.value : 0;
    this.NaExchTarget.value = this.CEC.value * assumptions.targetESP.value / 100;
    this.NaReduction.value = this.sodium.value - this.NaExchTarget.value;
    this.meqNaTreated.value = this.NaReduction.value * 1000 * 1000 / 100 * assumptions.soilMassSodicity.value[layer];
    this.molesNaTreated.value = this.meqNaTreated.value / 1000;
    this.molesCaNeeded.value = this.molesNaTreated.value / 2;
    this.molecularWeightGypsum.value = 40 + 32 + (16 * 4) + 2 * (2 + 16);
    this.weightGypsum.value = this.molecularWeightGypsum.value * this.molesCaNeeded.value;
    this.massPureGypsum.value = this.weightGypsum.value / 1000000;
    this.massCommericalGypsum.value = assumptions.gypsumEfficiencyFactor.value != 0 ?
      Math.max(0, this.massPureGypsum.value / (assumptions.gypsumPurity.value / 100)) : 0;

  }

  calcNutrients(assumptions, layer) {
    this.textureFactor.value = this.currentGrade.ECFactor;
    //this.clayContent.value = this.currentGrade.avClayContent / 100;
    this.clayContent.value = this.currentGrade.avClayContent;

    this.CCR.value = this.CEC.value / (this.clayContent.value / 100) / 100;

    // Select mineralogy
    for (var i = 0; i < this.mineralogy.values.length; i++) {
      if (this.CCR.value >= this.mineralogy.values[i].CCRLow && this.CCR.value < this.mineralogy.values[i].CCRHigh) {
        this.currentMineralogy = this.mineralogy.values[i];
        this.mineralogyName.value = this.currentMineralogy.code;
      }
    }

    this.conductivityECe.value = this.conductivity.value * this.textureFactor.value;

    this.CaMgRatio.value = this.magnesium.value != 0 ? this.calcium.value / this.magnesium.value : 0;

    this.CNRatio.value = this.totalN.value != 0 ? (this.organicCarbon.value / 100) / (this.totalN.value / 1000000) : 0;

    this.nutrientRequirements = new NutrientRequirements(this.pHWater.value, this.currentGrade.group.name);
    this.sodium.required = 0;

    this.potassium.required = this.potassium.value < this.nutrientRequirements.potassium.threshold ?
      ((this.nutrientRequirements.potassium.threshold - this.potassium.value) * 10 *
        this.nutrientRequirements.potassium.mwc) / 1000 * assumptions.soilMassNutrients.value : 0;

    this.magnesium.required = this.magnesium.value < this.nutrientRequirements.magnesium.threshold ?
      ((this.nutrientRequirements.magnesium.threshold - this.magnesium.value) * 10 *
        this.nutrientRequirements.magnesium.mwc) / 1000 * assumptions.soilMassNutrients.value : 0;

    this.calcium.required = this.calcium.value < this.nutrientRequirements.calcium.threshold ?
      ((this.nutrientRequirements.calcium.threshold - this.calcium.value) * 10 *
        this.nutrientRequirements.calcium.mwc) / 1000 * assumptions.soilMassNutrients.value : 0;

    this.nitrateN.required = this.nitrateN.value < this.nutrientRequirements.nitrateN.threshold ?
      (this.nutrientRequirements.nitrateN.threshold - this.nitrateN.value) / 1000 *
      assumptions.soilMassNutrients.value : 0;

    this.totalN.required = this.totalN.value < this.nutrientRequirements.totalN.threshold ?
      (this.nutrientRequirements.totalN.threshold - this.totalN.value) / 1000 *
      assumptions.soilMassNutrients.value : 0;

    // this.phosphorus.required = this.phosphorus.value < this.currentGrade.group.availablePThreshold ?
    //   (this.currentGrade.group.availablePThreshold - this.phosphorus.value) / 1000 *
    //   assumptions.soilMassNutrients.value[layer] : 0;

    this.organicCarbon.required = this.organicCarbon.value < this.nutrientRequirements.organicCarbon.threshold ?
      (this.nutrientRequirements.organicCarbon.threshold - this.organicCarbon.value) * 10000 / 1000 *
      assumptions.soilMassNutrients.value : 0;

    this.organicMatter.required = this.organicMatter.value < this.nutrientRequirements.organicMatter.threshold ?
      (this.nutrientRequirements.organicMatter.threshold - this.organicMatter.value) * 10000 / 1000 *
      assumptions.soilMassNutrients.value : 0;

    this.potassiumAvail.required = this.potassiumAvail.value < this.nutrientRequirements.potassiumAvail.threshold ?
      (this.nutrientRequirements.potassiumAvail.threshold - this.potassiumAvail.value) / 1000 *
      assumptions.soilMassNutrients.value : 0;

    this.phosphorusAvail.required = this.phosphorusAvail.value < this.nutrientRequirements.phosphorusAvail.threshold ?
      (this.nutrientRequirements.phosphorusAvail.threshold - this.phosphorusAvail.value) / 1000 *
      assumptions.soilMassNutrients.value : 0;

    this.sulphurAvail.required = this.sulphurAvail.value < this.nutrientRequirements.sulphurAvail.threshold ?
      (this.nutrientRequirements.sulphurAvail.threshold - this.sulphurAvail.value) / 1000 *
      assumptions.soilMassNutrients.value : 0;

    // this.sulphate.required = this.sulphate.value < this.nutrientRequirements.sulphate.threshold ?
    //   (this.nutrientRequirements.sulphate.threshold - this.sulphate.value) / 1000 *
    //   assumptions.soilMassNutrients.value[layer] : 0;

    this.boron.required = this.boron.value < this.nutrientRequirements.boron.threshold ?
      (this.nutrientRequirements.boron.threshold - this.boron.value) / 1000 *
      assumptions.soilMassNutrients.value : 0;

    this.zinc.required = this.zinc.value < this.nutrientRequirements.zinc.threshold ?
      (this.nutrientRequirements.zinc.threshold - this.zinc.value) / 1000 *
      assumptions.soilMassNutrients.value : 0;

    this.copper.required = this.copper.value < this.nutrientRequirements.copper.threshold ?
      (this.nutrientRequirements.copper.threshold - this.copper.value) / 1000 *
      assumptions.soilMassNutrients.value : 0;

    this.manganese.required = this.manganese.value < this.nutrientRequirements.manganese.threshold ?
      (this.nutrientRequirements.manganese.threshold - this.manganese.value) / 1000 *
      assumptions.soilMassNutrients.value : 0;
  }

  setClasses() {

    //var sampleMember = sample.values[i][j];
    // var descriptor = soilDescriptor.values[i].members[j];
    var sampleTexture = this.currentGrade.group.name;
    var samplePh = this.pHWater.value;

    // Set the phosphorus classes by soil
    if (sampleTexture == "Sands" || sampleTexture == "Sandy Loam") {
      this.phosphorusAvail.currentClass = this.phosphorusAvail.classSet[0];
    } else if (sampleTexture == "Loams") {
      this.phosphorusAvail.currentClass = this.phosphorusAvail.classSet[1];
    } else if (sampleTexture == "Clay Loams") {
      this.phosphorusAvail.currentClass = this.phosphorusAvail.classSet[2];
    } else {
      this.phosphorusAvail.currentClass = this.phosphorusAvail.classSet[3];
    }

    this.phosphorusAvail.colourClass = this.phosphorusAvail.currentClass.colour;

    // Set the zinc classes by ph
    if (samplePh >= 7) {
      this.zinc.currentClass = this.zinc.classSet[0];
    } else {
      this.zinc.currentClass = this.zinc.classSet[1];
    }

    this.zinc.colourClass = this.zinc.currentClass.colour;

    this.values.forEach(function (group) {
      group.members.forEach(function (element) {
        if (element.classSet == undefined) {
          for (var i = 0; i < element.classes.length; i++) {
            if (element.classes[i].min == null) {
              if (element.value < element.classes[i].max) {
                element.colourClass = element.classes[i].colour;
              }
            } else if (element.classes[i].max == null) {
              if (element.value >= element.classes[i].min) {
                element.colourClass = element.classes[i].colour;
              }
            } else if (element.value > element.classes[i].min && element.value <= element.classes[i].max) {
              element.colourClass = element.classes[i].colour;
            }
          }
        }
      })
    })
  }
  // calcAverages(soilSamples) {
  //     //If this sample is calculating averages then it is the last one

  //     this.values.sod = 0;
  //     this.values.pot = 0;
  //     this.values.mag = 0;
  //     this.values.calc = 0;
  //     this.values.cec = 0;
  //     this.values.esp = 0;
  //     this.values.cmRatio = 0;
  //     this.values.ccr = 0;
  //     this.values.gypsReq = 0;
  //     this.values.sodReq = 0;
  //     this.values.potReq = 0;
  //     this.values.magReq = 0;
  //     this.values.caclReq = 0;
  //     this.values.nitrateN = 0;
  //     this.values.totalN = 0;
  //     this.values.p = 0;
  //     this.values.oc = 0;
  //     this.values.sulphate = 0;
  //     this.values.b = 0;
  //     this.values.zincGT7 = 0;
  //     this.values.zincLT7 = 0;
  //     this.values.cu = 0;
  //     this.values.mn = 0;

  //     var i;

  //     for (i = 0; i < soilSamples.length - 1; i++) {
  //         this.values.sod += soilSamples[i].values.sod / (soilSamples.length - 1);
  //         this.values.pot += soilSamples[i].values.pot / (soilSamples.length - 1);
  //         this.values.mag += soilSamples[i].values.mag / (soilSamples.length - 1);
  //         this.values.calc += soilSamples[i].values.calc / (soilSamples.length - 1);
  //         this.values.cec += soilSamples[i].values.cec / (soilSamples.length - 1);
  //         this.values.esp += soilSamples[i].values.esp / (soilSamples.length - 1);
  //         this.values.cmRatio += soilSamples[i].values.cmRatio / (soilSamples.length - 1);
  //         this.values.ccr += soilSamples[i].values.ccr / (soilSamples.length - 1);
  //         this.values.gypsReq += soilSamples[i].values.gypsReq / (soilSamples.length - 1);
  //         this.values.sodReq += soilSamples[i].values.sodReq / (soilSamples.length - 1);
  //         this.values.potReq += soilSamples[i].values.potReq / (soilSamples.length - 1);
  //         this.values.magReq += soilSamples[i].values.magReq / (soilSamples.length - 1);
  //         this.values.caclReq += soilSamples[i].values.caclReq / (soilSamples.length - 1);
  //         this.values.nitrateN += soilSamples[i].values.nitrateN / (soilSamples.length - 1);
  //         this.values.totalN += soilSamples[i].values.totalN / (soilSamples.length - 1);
  //         this.values.p += soilSamples[i].values.p / (soilSamples.length - 1);
  //         this.values.oc += soilSamples[i].values.oc / (soilSamples.length - 1);
  //         this.values.sulphate += soilSamples[i].values.sulphate / (soilSamples.length - 1);
  //         this.values.b += soilSamples[i].values.b / (soilSamples.length - 1);
  //         this.values.zincGT7 += soilSamples[i].values.zincGT7 / (soilSamples.length - 1);
  //         this.values.zincLT7 += soilSamples[i].values.zincLT7 / (soilSamples.length - 1);
  //         this.values.cu += soilSamples[i].values.cu / (soilSamples.length - 1);
  //         this.values.mn += soilSamples[i].values.mn / (soilSamples.length - 1);
  //     }
  //}
}
