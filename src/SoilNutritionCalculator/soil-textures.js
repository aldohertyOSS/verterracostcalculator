export class SoilTextures {
    constructor() {

        //Groups

        this.sands = {
            "name": "Sands", "clayContent": "0-10%", "cohesionPlasticity": "Little or none", "feelSound": "Gritty feel, rasping sound", "rodOrRing": "Nil", "ribbonLength": "Negligible", "availablePThreshold":20
        };
        this.sandyLoams = {
            "name": "Sandy Loams", "clayContent": "10-20%", "cohesionPlasticity": "Slight (forms soft cohesive ball)", "feelSound": "Gritty feel, rasping sound", "rodOrRing": "Nil", "ribbonLength": "Up to 2.5cm", "availablePThreshold":20
        };
        this.loams = {
            "name": "Loams", "clayContent": "20-30%", "cohesionPlasticity": "Coheres", "feelSound": "May feel spongy or silky, no sound", "rodOrRing": "Forms rod but not ring", "ribbonLength": "2.5-4.0cm", "availablePThreshold":30
        };
        this.clayLoams = {
            "name": "Clay Loams", "clayContent": "30-35%", "cohesionPlasticity": "Coherent", "feelSound": "Smooth, no sound", "rodOrRing": "Forms rod but not ring", "ribbonLength": "4.0-5.0cm", "availablePThreshold":40
        };
        // this.lightClays = {
        //     "name": "Light Clays", "clayContent": "35-45%", "cohesionPlasticity": "Coherent and plastic", "feelSound": "Smooth and sticky", "rodOrRing": "Forms both rod and ring", "ribbonLength": "5.0-7.5cm", "availablePThreshold":60
        // };
        this.mediumClays = {
            "name": "Medium Clays", "clayContent": "45%-55%", "cohesionPlasticity": "Very coherent, like plasticine", "feelSound": "Smooth and sticky", "rodOrRing": "Forms both rod and ring", "ribbonLength": "7.5cm+", "availablePThreshold":70
        };
        this.heavyClays = {
            "name": "Heavy Clays", "clayContent": "55%+", "cohesionPlasticity": "Very coherent, like plasticine", "feelSound": "Smooth and sticky", "rodOrRing": "Forms both rod and ring", "ribbonLength": "7.5cm+", "availablePThreshold":80
        };

        //Grades
        this.values = [
            { "grade": "Sand", "avClayContent": 2.5, "ECFactor": 23, "group": this.sands },
            { "grade": "Loamy sand", "avClayContent": 5, "ECFactor": 23, "group": this.sands },
            { "grade": "Sandy loam", "avClayContent": 12.5, "ECFactor": 14, "group": this.sandyLoams },
            { "grade": "Fine sandy loam", "avClayContent": 12.5, "ECFactor": 14, "group": this.sandyLoams },
            { "grade": "Light sandy clay loam", "avClayContent": 17.5, "ECFactor": 14, "group": this.sandyLoams },
            { "grade": "Loam", "avClayContent": 25, "ECFactor": 9.5, "group": this.loams },
            { "grade": "Loam, fine sandy", "avClayContent": 0.25, "ECFactor": 9.5, "group": this.loams },
            { "grade": "Silt loam", "avClayContent": 25, "ECFactor": 9.5, "group": this.loams },
            { "grade": "Sandy clay loam", "avClayContent": 25, "ECFactor": 9.5, "group": this.loams },
            { "grade": "Clay loam", "avClayContent": 32.5, "ECFactor": 8.6, "group": this.clayLoams },
            { "grade": "Silty clay loam", "avClayContent": 32.5, "ECFactor": 8.6, "group": this.clayLoams },
            { "grade": "Fine sandy clay loam", "avClayContent": 32.5, "ECFactor": 8.6, "group": this.clayLoams },
            { "grade": "Sandy clay", "avClayContent": 37.5, "ECFactor": 8.6, "group": this.clayLoams },
            { "grade": "Silty clay", "avClayContent": 37.5, "ECFactor": 8.6, "group": this.clayLoams },
            { "grade": "Light clay", "avClayContent": 37.5, "ECFactor": 8.6, "group": this.clayLoams },
            { "grade": "Light medium clay", "avClayContent": 42.5, "ECFactor": 8.6, "group": this.clayLoams },
            { "grade": "Medium clay", "avClayContent": 50, "ECFactor": 7.5, "group": this.mediumClays },
            { "grade": "Heavy clay", "avClayContent": 55, "ECFactor": 5.8, "group": this.heavyClays }
        ]
    }


    //         this.values = [
    //     {
    //         "name": "Sands", "clayContent": "0-10%", "cohesionPlasticity": "Little or none", "feelSound": "Gritty feel, rasping sound", "rodOrRing": "Nil", "ribbonLength": "Negligible", "grades": [
    //             { "grade": "Sand", "avClayContent": 2.5, "ECFactor": 23 },
    //             { "grade": "Loamy sand", "avClayContent": 5, "ECFactor": 23 },
    //             { "grade": "Clayey sand", "avClayContent": 7.5, "ECFactor": 23 }
    //         ]
    //     },
    //     {
    //         "name": "Sandy Loams", "clayContent": "10-20%", "cohesionPlasticity": "Slight (forms soft cohesive ball)", "feelSound": "Gritty feel, rasping sound", "rodOrRing": "Nil", "ribbonLength": "Up to 2.5cm", "grades": [
    //             { "grade": "Sandy loam", "avClayContent": 12.5, "ECFactor": 14 },
    //             { "grade": "Fine sandy loam", "avClayContent": 12.5, "ECFactor": 14 },
    //             { "grade": "Light sandy clay loam", "avClayContent": 17.5, "ECFactor": 14 }
    //         ]
    //     },
    //     {
    //         "name": "Loams", "clayContent": "20-30%", "cohesionPlasticity": "Coheres", "feelSound": "May feel spongy or silky, no sound", "rodOrRing": "Forms rod but not ring", "ribbonLength": "2.5-4.0cm", "grades": [
    //             { "grade": "Loam", "avClayContent": 25, "ECFactor": 9.5 },
    //             { "grade": "Loam, fine sandy", "avClayContent": 0.25, "ECFactor": 9.5 },
    //             { "grade": "Silt loam", "avClayContent": 25, "ECFactor": 9.5 },
    //             { "grade": "Sandy clay loam", "avClayContent": 25, "ECFactor": 9.5 }
    //         ]
    //     },
    //     {
    //         "name": "Clay Loams", "clayContent": "30-35%", "cohesionPlasticity": "Coherent", "feelSound": "Smooth, no sound", "rodOrRing": "Forms rod but not ring", "ribbonLength": "4.0-5.0cm", "grades": [
    //             { "grade": "Clay loam", "avClayContent": 32.5, "ECFactor": 8.6 },
    //             { "grade": "Silty clay loam", "avClayContent": 32.5, "ECFactor": 8.6 },
    //             { "grade": "Fine sandy clay loam", "avClayContent": 32.5, "ECFactor": 8.6 }
    //         ]
    //     },
    //     {
    //         "name": "Light Clays", "clayContent": "35-45%", "cohesionPlasticity": "Coherent and plastic", "feelSound": "Smooth and sticky", "rodOrRing": "Forms both rod and ring", "ribbonLength": "5.0-7.5cm", "grades": [
    //             { "grade": "Sandy clay", "avClayContent": 37.5, "ECFactor": 8.6 },
    //             { "grade": "Silty clay", "avClayContent": 37.5, "ECFactor": 8.6 },
    //             { "grade": "Light clay", "avClayContent": 37.5, "ECFactor": 8.6 },
    //             { "grade": "Light medium clay", "avClayContent": 42.5, "ECFactor": 8.6 }
    //         ]
    //     },
    //     {
    //         "name": "Medium Clays", "clayContent": "45%-55%", "cohesionPlasticity": "Very coherent, like plasticine", "feelSound": "Smooth and sticky", "rodOrRing": "Forms both rod and ring", "ribbonLength": "7.5cm+", "grades": [
    //             { "grade": "Medium clay", "avClayContent": 50, "ECFactor": 7.5 }
    //         ]
    //     },
    //     {
    //         "name": "Heavy Clays", "clayContent": "55%+", "cohesionPlasticity": "Very coherent, like plasticine", "feelSound": "Smooth and sticky", "rodOrRing": "Forms both rod and ring", "ribbonLength": "7.5cm+", "grades": [
    //             { "grade": "Heavy clay", "avClayContent": 55, "ECFactor": 5.8 }
    //         ]
    //     }
    // ];
    // }
}
