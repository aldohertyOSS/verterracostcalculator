import { inject } from 'aurelia-framework';
import { ValueTable } from './value-table';
// import 'fetch';

@inject(ValueTable)
export class Infrastructure {

    constructor(ValueTable) {
        this.valueTable = ValueTable;
        this.values = this.valueTable.values;
    };

    activate() {
    }
}
