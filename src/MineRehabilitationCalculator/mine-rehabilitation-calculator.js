
// Converts from degrees to radians.
Math.radians = function (degrees) {
    return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.degrees = function (radians) {
    return radians * 180 / Math.PI;
};

import { ObserverLocator } from 'aurelia-framework';
import { bindable } from 'aurelia-framework';
import { inject } from 'aurelia-framework';

@inject(ObserverLocator)

export class MineRehabilitationCalculator {
    constructor(observerLocator) {
        this.observerLocator = observerLocator;

        // Rehab Area
        this.area = { "label": "Area", "units": "(ha)", "value": 8 };
        this.slope = { "label": "Slope", "units": "(degrees)", "value": 25 };

        this.inputs = {
            "label": "Target Area", "vars": [
                this.area,
                this.slope]
        };

        // Model Inputs
        this.slopeLength = { "label": "Slope length", "units": "(from imagery) (m)", "value": 330 };
        this.slopeAngle = { "label": "Slope angle", "units": "(degrees)", "value": 20 };
        this.ripDepth = { "label": "Rip depth", "units": "(cm)", "value": 150 };
        this.landUse = { "label": "Land Use", "units": "(Grazing/Natural)", "value": "Grazing", "possibilities": ["Grazing", "Natural"] };
        this.ameliorationAmount = { "label": "Amelioration amount", "units": "(none/.5/.75/1)", "value": 1, "possibilities": [0, 0.5, 0.75, 1] };
        this.ameliorationType = { "label": "Amelioration Type", "units": "(Lime/Gypsum)", "value": "Gypsum", "possibilities": ["Lime", "Gypsum"] };
        this.topSoilDepth = { "label": "Top soil depth", "units": "(cm)", "value": 20 };
        this.contourBankHeight = { "label": "Contour Bank height", "units": "()", "value": 500 };
        this.fertiliserAddition = { "label": "Fertiliser addition", "units": "(Yes/No)", "value": "Yes", "possibilities": ["Yes", "No"] };

        this.model = {
            "label": "Model Input", "vars": [
                this.slopeLength,
                this.slopeAngle,
                this.ripDepth,
                this.landUse,
                this.ameliorationAmount,
                this.ameliorationType,
                this.topSoilDepth,
                this.contourBankHeight,
                this.fertiliserAddition]
        };
        // Results
        this.slopeArea = { "label": "Slope area", "units": "(ha)", "value": 0 };
        this.bulkVolume = { "label": "Approx. Bulk Volume to be moved", "units": "(m3)", "value": 0 };
        this.cleanup = { "label": "Cleanup", "units": "", "value": 0 };
        this.structuralWorksCost = { "label": "Structural works", "units": "", "value": 0 };
        this.rippingCost = { "label": "Ripping", "units": "", "value": 0 };
        this.topSoilCost = { "label": "Top soil", "units": "", "value": 0 };
        this.seedingCost = { "label": "Seeding", "units": "", "value": 0 };
        this.ameliorationCost = { "label": "Amelioration", "units": "", "value": 0 };
        this.fertiliserCost = { "label": "Fertiliser", "units": "", "value": 0 };
        this.bulkPushingCost = { "label": "Bulk pushing", "units": "", "value": 0 };
        this.rehabilitationCost = { "label": "Rehabilitation", "units": "", "value": 0 };
        this.totalCost = { "label": "Total", "units": "", "value": 0 };

        this.results = {
            "label": "Results", "vars": [
                this.slopeArea,
                this.bulkVolume,
                this.cleanup,
                this.structuralWorksCost,
                this.rippingCost,
                this.topSoilCost,
                this.seedingCost,
                this.ameliorationCost,
                this.fertiliserCost,
                this.bulkPushingCost,
                this.rehabilitationCost,
                this.totalCost]
        };


        this.bulkMovePrice = { "label": "Bulk move", "units": "(per m3)", "value": 1.5 };
        this.cleanupPrice = { "label": "Cleanup after bulk move", "units": "(per ha)", "value": 3900 };
        this.structuralWorksPrice = { "label": "Structural works (2000)", "units": "(per slope ha)", "value": 1600 };
        this.deepRipPrice = { "label": "Deep rip (150)", "units": "(per slope ha)", "value": 960 };
        this.topSoilPrice = { "label": "Top soil (20)", "units": "(per m3)", "value": 5 };
        this.pastureGrassPrice = { "label": "Pasture grass", "units": "(per slope ha)", "value": 1240 };
        this.nativesPrice = { "label": "Natives", "units": "(per slope ha)", "value": 2095 };
        this.limePrice = { "label": "Lime", "units": "(per slope ha)", "value": 860 };
        this.gypsumPrice = { "label": "Gypsum", "units": "(per slope ha)", "value": 125 };
        this.fertiliserPrice = { "label": "Fertiliser (pasture)", "units": "(per ha)", "value": 420 };

        //Prices
        this.costs = {
            "label": "Costs", "vars": [
                this.bulkMovePrice,
                this.cleanupPrice,
                this.structuralWorksPrice,
                this.deepRipPrice,
                this.topSoilPrice,
                this.pastureGrassPrice,
                this.nativesPrice,
                this.limePrice,
                this.gypsumPrice,
                this.fertiliserPrice]
        };

        this.visInputs = [
            this.inputs,
            this.model,
            this.costs
        ]
    }

    onChange(newVal, oldVal) {
        this.calculate();
    }

    addWatch(element) {
        var subscription = this.observerLocator
            .getObserver(element, 'value')
            .subscribe((newValue, oldValue) => this.onChange(newValue, oldValue));

    }
    attached() {
        //   var subscription = this.observerLocator
        //     .getObserver(this.inputs.vars[0], 'value')
        //     .getObserver(this.inputs.vars[1], 'value')
        //     .subscribe((newValue, oldValue) => this.onChange(newValue, oldValue));
        this.inputs.vars.forEach(function (element) {
            this.addWatch(element);
        }, this);

        this.model.vars.forEach(function (element) {
            this.addWatch(element);
        }, this);

        this.costs.vars.forEach(function (element) {
            this.addWatch(element);
        }, this);

        this.calculate();
    }

    formatCurrency(value) {
        var val = Math.round(value);
        return val.toLocaleString('currency');
    }

    calculate() {

        var slopeCurrRads = Math.radians(this.slope.value);
        var slopeAngleRads = Math.radians(this.slopeAngle.value);

        this.slopeArea.value = this.area.value / Math.cos(slopeCurrRads);;
        this.bulkVolume.value = ((((Math.tan(slopeCurrRads) * this.slopeLength.value) * this.slopeLength.value) / 2) -
            (((Math.tan(slopeAngleRads) * this.slopeLength.value) * this.slopeLength.value) / 2)) *
            ((this.area.value * 10000) / this.slopeLength.value);

        this.cleanup.value = this.cleanupPrice.value * this.area.value;
        this.structuralWorksCost.value = (0.8 * this.contourBankHeight.value) * this.slopeArea.value;
        this.rippingCost.value = 6.4 * this.ripDepth.value * this.slopeArea.value;
        this.topSoilCost.value = ((this.slopeArea.value * 10000) * (this.topSoilDepth.value / 100)) * this.topSoilPrice.value;
        this.seedingCost.value = this.landUse.value == "Natural" ? this.slopeArea.value * this.nativesPrice.value : this.slopeArea.value * this.pastureGrassPrice.value;
        this.ameliorationCost.value = this.ameliorationType.value == "Lime" ? this.slopeArea.value * this.ameliorationAmount.value * this.limePrice.value : this.slopeArea.value * this.ameliorationAmount.value * this.gypsumPrice.value;
        this.fertiliserCost.value = this.fertiliserAddition.value == "Yes" ? this.slopeArea.value * this.fertiliserPrice.value : 0;

        this.bulkPushingCost.value = this.bulkVolume.value * this.bulkMovePrice.value;
        this.rehabilitationCost.value = 0;
        var i = 0;
        for (i = 2; i <= 8; i++) {
            this.rehabilitationCost.value += this.results.vars[i].value;
        }
        this.totalCost.value = this.bulkPushingCost.value + this.rehabilitationCost.value;

        console.log(this.results.vars);
    }
}