export class App {
  configureRouter(config, router) {
    config.title = '';
    config.map([
      // { route: ['', 'welcome'], name: 'welcome',      moduleId: 'welcome',      nav: true, title: 'Welcome' },
      // { route: 'users',         name: 'users',        moduleId: 'users',        nav: true, title: 'Github Users' },
      // { route: 'child-router',  name: 'child-router', moduleId: 'child-router', nav: true, title: 'Child Router' },
      { route: ['', 'value-table'], name: 'value-table', moduleId: 'value-table', nav: true, title: 'Table of Values' },
      { route: 'infrastructure', name: 'infrastructure', moduleId: 'infrastructure', nav: true, title: 'Infrastructure' },
      { route: 'soil-nutrition-calculator', name: 'soil-nutrition-calculator', moduleId: 'SoilNutritionCalculator/soil-nutrition-calculator', nav: true, title: 'Soil Nutrition' },
      { route: 'mine-rehabilitation-calculator', name: 'mine-rehabilitation-calculator', moduleId: 'MineRehabilitationCalculator/mine-rehabilitation-calculator', nav: true, title: 'Mine Rehab' },
      { route: 'climate-characteristics-calculator', name: 'climate-characteristics-calculator', moduleId: 'ClimateCalculator/climate-characteristics-calculator', nav: true, title: 'Climate Characteristics' }
    ]);

    this.router = router;
  }
}
