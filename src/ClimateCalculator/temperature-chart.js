
import { inject } from 'aurelia-framework';
import {bindable, bindingMode} from 'aurelia-framework';
import { ObserverLocator } from 'aurelia-binding';
import Highcharts from 'highcharts';
//import Exporting from "highcharts/modules/exporting";
//Exporting(Highcharts);
import { EventAggregator } from 'aurelia-event-aggregator';

@inject(EventAggregator)

export class TemperatureChartCustomElement {
    @bindable metdata;
    
    constructor(eventAggregator) {
        this.ea = eventAggregator;

        var datasubscription = this.ea.subscribe('dataUpdated', message => this.onDataChange());
    }

    onDataChange() {
        this.doChart();
    }

    bind(){
        
    }

    attached(){
        this.doChart();
    }

    doChart() {

        var maxT = this.metdata.find(x=>x.name==="MaxT");
        var minT = this.metdata.find(x=>x.name==="MinT");
        
        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'temperature-chart-holder',
                animation: false,
                },

            title: {
                text: 'Temperature'
            },

            xAxis: {
                //categories: ['January', 'February',' March' ,'April' ,'May' ,'June', 'July', 'August' ,'September', 'October', 'November', 'December'],
                categories: ['Jan', 'Feb',' Mar' ,'Apr' ,'May' ,'Jun', 'Jul', 'Aug' ,'Sep', 'Oct', 'Nov', 'Dec'],
                title:
                {
                    text: 'Month'
                },

                tickInterval: 1
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Temperature (oC)',
                    margin: 20
                }
            },
            plotOptions: {
                column: {
                    //grouping: stack
                    //stacking: 'normal'
                },
                series: {
                    animation: true
                }
            },

            tooltip: {
                pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}:' + '<b>{point.y:.2f}</b><br/>',

            },

            series: [
                {
                    type:'line',
                    color: '#4472C4',
                    data: maxT.data,
                    name: maxT.title,
                     marker: {
                        enabled: false
                    },

                },
                {
                    type:'line',
                    color: '#ED7D31',
                    data: minT.data,
                    name: minT.title,
                     marker: {
                        enabled: false
                    },

                
                }
            ]
        });
    }
}