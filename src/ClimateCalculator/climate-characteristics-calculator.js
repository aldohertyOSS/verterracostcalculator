import {
  ObserverLocator
} from 'aurelia-framework';
import {
  bindable,
  bindingMode
} from 'aurelia-framework';
import {
  inject
} from 'aurelia-framework';
import {
  HttpClient
} from 'aurelia-fetch-client';
import {
  EventAggregator
} from 'aurelia-event-aggregator';

@inject(ObserverLocator, EventAggregator)

export class ClimateCharacteristicsCalculator {

  metdata = [];

  constructor(observerLocator, eventAggregator) {

    this.observerLocator = observerLocator;
    this.ea = eventAggregator;
    this.dataLoaded = true;

    this.locationVars = [{
        name: "Latitude",
        units: "°",
        hint: "Latitude in decimal degrees (South is -ve)",
        value: -20.55
      },
      {
        name: "Longitude",
        units: "°",
        value: 147.85,
        hint: "Longitude in decimal degrees"
      }
    ]

    this.rainfallMethod = 0;

    this.months = [{
        id: 1,
        name: "January",
        days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
      },
      {
        id: 2,
        name: "February",
        days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28]
      },
      {
        id: 3,
        name: "March",
        days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
      },
      {
        id: 4,
        name: "April",
        days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]
      },
      {
        id: 5,
        name: "May",
        days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
      },
      {
        id: 6,
        name: "June",
        days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]
      },
      {
        id: 7,
        name: "July",
        days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
      },
      {
        id: 8,
        name: "August",
        days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
      },
      {
        id: 9,
        name: "September",
        days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]
      },
      {
        id: 10,
        name: "October",
        days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
      },
      {
        id: 11,
        name: "November",
        days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]
      },
      {
        id: 12,
        name: "December",
        days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
      },
    ]

    this.selectedMonth = this.months[0];
    this.selectedDay = this.selectedMonth.days[0];

    this.IFDClass = null;

    this.metdata = [{
        name: "MonthlyRainfall",
        title: "Average monthly rainfall (mm)",
        data: [],
        summary: 0
      },
      {
        name: "MonthlyRainfallD1",
        title: "Decile 1 monthly rainfall (mm)",
        data: [],
        summary: 0
      },
      {
        name: "MonthlyRainfallD9",
        title: "Decile 9 monthly rainfall (mm)",
        data: [],
        summary: 0
      },
      // {
      //   name: "DailyEvaporation",
      //   title: "Mean daily evaporation (mm) (optional)",
      //   data: [6.2, 5.7, 5.2, 4.4, 3.5, 2.9, 3.2, 4.1, 5.3, 6.5, 6.9, 6.9]
      // },
      {
        name: "MonthlyEvaporation",
        title: "Mean monthly evap. (mm)",
        data: [],
        summary: 0
      },
      {
        name: "MaxT",
        title: "Mean maximum temperature (oC)",
        data: [],
        summary: 0
      },
      {
        name: "MinT",
        title: "Mean minimum termperature (oC)",
        data: [],
        summary: 0
      }
    ];

    this.IFDClasses = [{
        AEP: 63.2,
        class: "AEP50",
        class2: "Very High",
        color: "#FF0000"
      },
      {
        AEP: 50,
        class: "AEP50",
        class2: "Very High",
        color: "#FF0000"
      },
      {
        AEP: 20,
        class: "AEP50",
        class2: "High",
        color: "#FFC000"
      },
      {
        AEP: 10,
        class: "AEP10",
        class2: "High",
        color: "#FFC000"
      },
      {
        AEP: 5,
        class: "AEP5",
        class2: "Moderate",
        color: "#FFFF00"
      },
      {
        AEP: 2,
        class: "AEP5",
        class2: "Moderate",
        color: "#FFFF00"
      },
      {
        AEP: 1,
        class: "AEP1",
        class2: "Low",
        color: "#92D050"
      }
    ];

    this.IFDData = [{
        name: "AEPX",
        title: "Annual exceedance probability AEP (1 in x)",
        data: [1.58, 2, 5, 10, 20, 50, 100]
      },
      {
        name: "AEPPC",
        title: "Annual exceedance probability (AEP) (%)",
        data: [63.2, 50, 20, 10, 5, 2, 1]
      },
      {
        name: "ARI",
        title: "Annual return interval (ARI)",
        data: [1, 1.44, 4.48, 9.49, 20, 50, 100]
      },
      {
        name: "EY",
        title: "Exceedance per year (EY)",
        data: [1, 0.69, 0.22, 0.11, 0.05, 0.02, 0.01]
      },
      {
        name: "OHD",
        title: "1 hour duration",
        data: []
      }
    ];

    // this.latitude = -20.55;
    // this.longitude = 147.85;

    this.rainfall2Month = 0;

    this.yearRainfalls = null;


    //Downloaded data
    this.DownloadData = JSON.parse('{"data":[{"name":"MaxT","data":[36.4,35.6,33.9,31.9,30.2,26.5,26.2,29,31.5,33.7,35,37]},{"name":"MinT","data":[18.4,19.4,17.7,13.7,9.6,6.1,5,6.5,9.8,13.9,17.7,19.1]},{"name":"Evap","data":[195.19318181818187,161.8295454545454,165.76590909090905,136.41136363636366,110.52727272727272,91.03636363636362,101.77272727272734,127.11818181818181,161.89545454545456,200.14318181818192,206.76590909090902,213.47727272727272]},{"name":"Rain","data":[146.41590909090908,156.7306818181818,100.77727272727276,42.26363636363636,34.97272727272727,30.989772727272733,21.89090909090909,16.83295454545455,11.798863636363638,23.55113636363637,53.02272727272726,99.66818181818185]},{"name":"Rain10","data":[43.22,31.460000000000004,13.780000000000001,1.5,2.58,1.4200000000000004,0,0,0,0,1.8000000000000003,24.5]},{"name":"Rain90","data":[286.84000000000003,330.14000000000004,203.9,86.78000000000003,76.86000000000003,61.12,63.02000000000001,40.540000000000006,35.00000000000001,55.120000000000026,134.52,196.08]},{"name":"IFD","data":[34.2,39,52.9,61.5,69.3,78.8,85.5]}]}');

    this.populateDataContainers();


    this.addWatch("selectedMonth");
    this.addWatch("selectedDay");
    
  }

  fetchData() {
    let client = new HttpClient();
    //   HttpClient.configure(config => {
    //       config
    //         .withBaseUrl('api/')
    //         .withDefaults({
    //           credentials: 'same-origin',
    //           headers: {
    //             'Accept': 'application/json',
    //             'X-Requested-With': 'Fetch'
    //           }
    //         })
    //         .withInterceptor({
    //           request(request) {
    //             console.log(`Requesting ${request.method} ${request.url}`);
    //             return request;
    //           },
    //           response(response) {
    //             console.log(`Received ${response.status} ${response.url}`);
    //             return response;
    //           }
    //         });

    // client.configure(config => {
    //     config
    //       .useStandardConfiguration()
    //       .withBaseUrl('api/')
    //       .withDefaults({
    //         //mode:'no-cors',
    //         // credentials: 'no-cors',
    //         headers: {
    //           'X-Requested-With': 'Fetch'
    //         }
    //       });
    //     //   .withInterceptor({
    //     //     request(request) {
    //     //       let authHeader = fakeAuthService.getAuthHeaderValue(request.url);
    //     //       request.headers.append('Authorization', authHeader);
    //     //       return request;
    //     //     }
    //     //   });
    //   });

    $("body").css("cursor", "progress");

    var myHeaders = new Headers();
    var myInit = {
      method: 'GET',
      headers: myHeaders,
      //   mode: 'no-cors',
      cache: 'default'
    };

    client.fetch(window.location.origin.replace(":9000","") + ":6699/test.php?lat=" + this.locationVars[0].value + "&long=" + this.locationVars[1].value, myInit)
      .then(response =>  response.json())
      .then(data => {
        this.DownloadData = data;
        this.populateDataContainers();
        $("body").css("cursor", "default");
      })
  }

  RFClicked(option) {
    this.rainfallMethod = option;

    this.calcResults();
    // alert(this.pathway + ", Pathway");
  }

  calcResults() {
    this.calc2MonthRainfall();
    this.calcIFD();
  }

  calc2MonthRainfall() {
    var currentMonth = this.selectedMonth.id - 1;
    var previousMonth = currentMonth - 1;
    var previousPreviousMonth = currentMonth - 2;
    var nextMonth = currentMonth + 1;

    if (previousMonth < 0) {
      previousMonth += 12;
    }

    if (previousPreviousMonth < 0) {
      previousPreviousMonth += 12;
    }

    if (nextMonth > 11) {
      nextMonth -= 12;
    }

    if (this.rainfallMethod == 0) {
      this.yearRainfalls = this.metdata.find(x => x.name === "MonthlyRainfall");
    } else if (this.rainfallMethod == 1) {
      this.yearRainfalls = this.metdata.find(x => x.name === "MonthlyRainfallD1");
    } else {
      this.yearRainfalls = this.metdata.find(x => x.name === "MonthlyRainfallD9");
    }

    if (this.selectedDay <= 10) {
      this.rainfall2Month = this.yearRainfalls.data[previousMonth] + this.yearRainfalls.data[previousPreviousMonth]; 
    } else if (this.selectedDay > 10 && this.selectedDay <= 20) {
      this.rainfall2Month = this.yearRainfalls.data[currentMonth] / 2 + this.yearRainfalls.data[previousMonth] + this.yearRainfalls.data[previousPreviousMonth] / 2;
    } else {
      this.rainfall2Month = this.yearRainfalls.data[currentMonth] + this.yearRainfalls.data[previousMonth];                         
    }


  }
  calcIFD() {
    var IFDData = this.IFDData.find(x => x.name === "OHD")
    var targetRainfall = 50;

    var i = 0;
    //Find the first IFD category that is over the target
    for (i = 0; i < IFDData.data.length; i++) {
      if (IFDData.data[i] > targetRainfall) {
        break;
      }
    }

    this.IFDClass = this.IFDClasses[i];
  }

  formatValue(value, sigFigs) {
    var val = value.toFixed(sigFigs);

    return val.toLocaleString();

  }

  populateDataContainers() {


    this.metdata.find(x => x.name === "MonthlyRainfall").data = this.DownloadData.data.find(y => y.name === "Rain").data;
    this.metdata.find(x => x.name === "MonthlyRainfallD1").data = this.DownloadData.data.find(y => y.name === "Rain10").data;
    this.metdata.find(x => x.name === "MonthlyRainfallD9").data = this.DownloadData.data.find(y => y.name === "Rain90").data;
    this.metdata.find(x => x.name === "MonthlyEvaporation").data = this.DownloadData.data.find(y => y.name === "Evap").data;
    this.metdata.find(x => x.name === "MaxT").data = this.DownloadData.data.find(y => y.name === "MaxT").data;
    this.metdata.find(x => x.name === "MinT").data = this.DownloadData.data.find(y => y.name === "MinT").data;
    this.IFDData.find(x => x.name === "OHD").data = this.DownloadData.data.find(y => y.name === "IFD").data;

    //Calc summaries
    var current = this.metdata.find(x => x.name === "MonthlyRainfall");
    current.summary = this.calculateSum(current.data);

    current = this.metdata.find(x => x.name === "MonthlyRainfallD1");
    current.summary = this.calculateSum(current.data);

    current = this.metdata.find(x => x.name === "MonthlyRainfallD9");
    current.summary = this.calculateSum(current.data);

    current = this.metdata.find(x => x.name === "MonthlyEvaporation");
    current.summary = this.calculateSum(current.data);

    current = this.metdata.find(x => x.name === "MaxT");
    current.summary = this.calculateAverage(current.data);

    current = this.metdata.find(x => x.name === "MinT");
    current.summary = this.calculateAverage(current.data);

    current = this.IFDData.find(x => x.name === "OHD");
    current.summary = this.calculateSum(current.data);

    this.calcResults();

    this.ea.publish('dataUpdated', {});

    this.dataLoaded = true;

    $("body").css("cursor", "default");

  }

  onChange(newVal, oldVal) {
     this.calcResults();
  }

  addWatch(name) {
    var subscription = this.observerLocator
      .getObserver(this, name)
      .subscribe((newValue, oldValue) => this.onChange(newValue, oldValue));

  }

  attached() {
    //   var subscription = this.observerLocator
    //     .getObserver(this.inputs.vars[0], 'value')
    //     .getObserver(this.inputs.vars[1], 'value')
    //     .subscribe((newValue, oldValue) => this.onChange(newValue, oldValue));
    // this.inputs.vars.forEach(function (element) {
    //   this.addWatch(element);
    // }, this);

    // this.model.vars.forEach(function (element) {
    //   this.addWatch(element);
    // }, this);

    // this.costs.vars.forEach(function (element) {
    //   this.addWatch(element);
    // }, this);

    // this.calculate();

    // Watch the soils changed
    // this.metdata.forEach(function (element) {
    //   this.observerLocator
    //     .getObserver(element, 'data')
    //     .subscribe((newValue, oldValue) => this.onSampleChange(newValue, oldValue));
    // }, this);
  }

  formatCurrency(value) {
    // var val = Math.round(value);
    // return val.toLocaleString('currency');
  }

  calculatePercentile(percentile, values) {
    values.sort(function (a, b) {
      return a - b
    });
    var position = values.length * percentile;
    var index = Math.floor(position);
    var remainder = position - index;

    return ((1 - remainder) * values[index - 1] + remainder * values[index]);
  }

  calculateAverage(values) {
    return this.calculateSum(values) / values.length;
  }

  calculateSum(values) {
    var total = 0;

    values.forEach(value => {
      total += value;
    });

    return total;
  }

  calculate() {

    // var slopeCurrRads = Math.radians(this.slope.value);
    // var slopeAngleRads = Math.radians(this.slopeAngle.value);

    // this.slopeArea.value = this.area.value / Math.cos(slopeCurrRads);;
    // this.bulkVolume.value = ((((Math.tan(slopeCurrRads) * this.slopeLength.value) * this.slopeLength.value) / 2) -
    //     (((Math.tan(slopeAngleRads) * this.slopeLength.value) * this.slopeLength.value) / 2)) *
    //   ((this.area.value * 10000) / this.slopeLength.value);

    // this.cleanup.value = this.cleanupPrice.value * this.area.value;
    // this.structuralWorksCost.value = (0.8 * this.contourBankHeight.value) * this.slopeArea.value;
    // this.rippingCost.value = 6.4 * this.ripDepth.value * this.slopeArea.value;
    // this.topSoilCost.value = ((this.slopeArea.value * 10000) * (this.topSoilDepth.value / 100)) * this.topSoilPrice.value;
    // this.seedingCost.value = this.landUse.value == "Natural" ? this.slopeArea.value * this.nativesPrice.value : this.slopeArea.value * this.pastureGrassPrice.value;
    // this.ameliorationCost.value = this.ameliorationType.value == "Lime" ? this.slopeArea.value * this.ameliorationAmount.value * this.limePrice.value : this.slopeArea.value * this.ameliorationAmount.value * this.gypsumPrice.value;
    // this.fertiliserCost.value = this.fertiliserAddition.value == "Yes" ? this.slopeArea.value * this.fertiliserPrice.value : 0;

    // this.bulkPushingCost.value = this.bulkVolume.value * this.bulkMovePrice.value;
    // this.rehabilitationCost.value = 0;
    // var i = 0;
    // for (i = 2; i <= 8; i++) {
    //   this.rehabilitationCost.value += this.results.vars[i].value;
    // }
    // this.totalCost.value = this.bulkPushingCost.value + this.rehabilitationCost.value;

    // console.log(this.results.vars);
  }
}
