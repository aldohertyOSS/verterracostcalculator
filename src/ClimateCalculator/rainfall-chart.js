
import { inject } from 'aurelia-framework';
import {bindable, bindingMode} from 'aurelia-framework';
import { ObserverLocator } from 'aurelia-binding';
import Highcharts from 'highcharts';
//import Exporting from "highcharts/modules/exporting";
//Exporting(Highcharts);
import { EventAggregator } from 'aurelia-event-aggregator';

@inject(EventAggregator)

export class RainfallChartCustomElement {
    @bindable metdata;
    
    constructor(eventAggregator) {
        this.ea = eventAggregator;

        var datasubscription = this.ea.subscribe('dataUpdated', message => this.onDataChange());
    }

    onDataChange() {
        this.doChart();
    }

    bind(){
        
    }

    attached(){
        this.doChart();
    }

    doChart() {

        var rain = this.metdata.find(x=>x.name==="MonthlyRainfall");
        var rain10 = this.metdata.find(x=>x.name==="MonthlyRainfallD1");
        var rain90 = this.metdata.find(x=>x.name==="MonthlyRainfallD9");
        var evap = this.metdata.find(x=>x.name==="MonthlyEvaporation");
        
        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'rainfall-chart-holder',
                animation: false,
                },

            title: {
                text: 'Rainfall and Evaopration'
            },

            xAxis: {
                //categories: ['January', 'February',' March' ,'April' ,'May' ,'June', 'July', 'August' ,'September', 'October', 'November', 'December'],
                categories: ['Jan', 'Feb',' Mar' ,'Apr' ,'May' ,'Jun', 'Jul', 'Aug' ,'Sep', 'Oct', 'Nov', 'Dec'],
                title:
                {
                    text: 'Month'
                },

                tickInterval: 1
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Rainfall / Evaporation (mm/month)',
                    margin: 20
                }
            },
            plotOptions: {
                column: {
                    //grouping: stack
                    //stacking: 'normal'
                },
                series: {
                    animation: true
                }
            },

            tooltip: {
                pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}:' + '<b>{point.y:.2f}</b><br/>',

            },

            series: [
                {
                    type:'column',
                    color: '#4AC28A',
                    data: rain10.data,
                    name: rain.title,
                     marker: {
                        enabled: false
                    },

                },
                {
                    type:'column',
                    color: '#4472C4',
                    data: rain.data,
                    name: rain.title,
                     marker: {
                        enabled: false
                    },

                },
                
                {
                    type:'column',
                    color: '#A5A5A5',
                    data: rain90.data,
                    name: rain90.title,
                     marker: {
                        enabled: false
                    },
                },
                {
                    type:'line',
                    color: '#ED7D31',
                    data: evap.data,
                    name: evap.title,
                     marker: {
                        enabled: false
                    },

                }
            ]
        });
    }
}