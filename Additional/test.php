<?php
	
$input = filter_input_array(INPUT_GET);

$result = getMetStats($input['lat'], $input['long']);

$resultIFD = getIFDStats($input['lat'], $input['long']);

array_push($result["data"], Array("name"=>"IFD", "data" => $resultIFD));

echo(json_encode($result));
//print_r($input);
// if (!empty($input)) {
// 	if (isset($input['lat']) && isset($input['long'] ))
// 	{
//$response = getMetStats($input['lat'], $input['long']);

//getIFDStats($input['lat'], $input['long']);
//echo $response;
//return; //Data structure
//	}
//}


function getIFDStats($lat, $long)
{
	$ch = curl_init();
		
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, "http://www.bom.gov.au/water/designRainfalls/revised-ifd/?design=ifds&sdhr=true&nsd%5B%5D=60&nsdunit%5B%5D=m&coordinate_type=dd&latitude=".$lat."&longitude=".$long."&user_label=&values=depths&update=&year=2016");
	curl_setopt( $ch, CURLOPT_COOKIE, "acknowledgedConditions=true; acknowledgedCoordinateCaveat=true; ifdCookieTest=true; __utma=172860464.1053221097.1522989488.1522989488.1522989488.1; __utmc=172860464; __utmz=172860464.1522989488.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmt=1; __utmb=172860464.12.10.1522989488" ); 
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
	// grab URL and pass it to the browser
	$Data = curl_exec($ch);
		
	// close cURL resource, and free up system resources
	curl_close($ch);

	$TableRows = explode("</tr>", $Data);

	$IFDBlock;

	for($i =0; $i < sizeof($TableRows); $i++)
	{
		if(strpos($TableRows[$i], "ifdDur60") !== false) 
		{
			$IFDBlock = $TableRows[$i];
			break;
		}
	}

	$IFDs = parseIFDBlock($IFDBlock);

	return $IFDs;
}

function parseIFDBlock($data)
{
	$result = array();

	$data = explode(">", $data);

	for($i = 1; $i < sizeof($data); $i++)
	{
		if(strpos($data[$i], "</td") !== false)
		{
			array_push($result, floatval(str_replace("</td", "", $data[$i])));
		}
	}

	return $result;
}
function getMetStats($lat, $long)
{
	$ch = curl_init();
	
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, "https://legacy.longpaddock.qld.gov.au/cgi-bin/silo/DataDrillDataset.php?format=monthly&lat=".$lat."&lon=".$long."&start=19300101&finish=20180101&username=ORBISDOHERTY&password=AL4350");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

   	$MaxT = Array();
   	$MinT = Array();
   	$Evap = Array();
	$Rain = Array();
	   
   	for ($i = 0; $i < 12; $i++ )
   	{
	   	array_push($MaxT,array());
	   	array_push($MinT,array());
	   	array_push($Evap,array());
	   	array_push($Rain,array());
   	}

	$Data = str_getcsv(curl_exec($ch), "\n");
	curl_close($ch);
	$FoundData = false;

	foreach($Data as &$Row) 
	{
	 	$Row = splitLine($Row); //parse the items in rows
	    if($FoundData == true)
	    {
           	if(sizeof($Row) == 7)
            {
			$month = intval(substr($Row[0], -2));
			array_push($MaxT[$month - 1], floatval($Row[1]));
			array_push($MinT[$month - 1], floatval($Row[2]));
			array_push($Evap[$month - 1], floatval($Row[4]));
			array_push($Rain[$month - 1], floatval($Row[3]));
            }
	    }	
		else
			{
		   	if($Row[0] == "()")
		    {
			  	$FoundData = true;
		    } 
	    }
	}
	
	$MaxTSummary = Array();
	$MinTSummary = Array();
	$EvapSummary = Array();
 	$RainSummary = Array();
 	$Rain10Summary = Array();
	$Rain90Summary = Array();
	 
	for($i = 0; $i < 12; $i++)
	{
		array_push($MaxTSummary, max($MaxT[$i]));
		array_push($MinTSummary, min($MinT[$i]));
		array_push($EvapSummary, calcAverage($Evap[$i]));
		array_push($RainSummary, calcAverage($Rain[$i]));
		sort($Rain[$i]); 
		array_push($Rain10Summary, calcPercentile($Rain[$i], 0.1));
		array_push($Rain90Summary, calcPercentile($Rain[$i], 0.9));
	}
 
	$result = Array();
	array_push($result, Array("name"=>"MaxT", "data"=>$MaxTSummary));
	array_push($result, Array("name"=>"MinT", "data"=>$MinTSummary));
	array_push($result, Array("name"=>"Evap", "data"=>$EvapSummary));
	array_push($result, Array("name"=>"Rain", "data"=>$RainSummary));
	array_push($result, Array("name"=>"Rain10", "data"=>$Rain10Summary));
	array_push($result, Array("name"=>"Rain90", "data"=>$Rain90Summary));
	
	$reply = Array("data" => $result);
    
    return $reply;
    

	//return $Data;

	//return curl_exec($ch);
	return $Data;
	// if ( $option == 1 ) {
	// 	$data = [ 'a', 'b', 'c' ];
	// 	// will encode to JSON array: ["a","b","c"]
	// 	// accessed as example in JavaScript like: result[1] (returns "b")
	// } else {
	// 	$data = [ 'name' => 'God', 'age' => -1 ];
	// 	// will encode to JSON object: {"name":"God","age":-1}  
	// 	// accessed as example in JavaScript like: result.name or result['name'] (returns "God")
	// }
	
	// header('Content-type: application/json');
	// echo json_encode( $data );
}

function calcPercentile($values, $percentile)
{
	$position = sizeof($values) * $percentile;
    $index =floor($position);
    $remainder = $position - $index;

    return ((1 - $remainder) * $values[$index - 1] + $remainder * $values[$index]);
}

function calcAverage($values)
{
	$total = 0;
	foreach($values as &$value)
	{
		$total += $value;
	}

	return $total / sizeof($values);
}

function splitLine($data)
{
	while(strpos($data, "  ") !== false) 
	{
		$data = str_replace("  "," ", $data);
	}

	return explode(" ", $data);
}

?>
